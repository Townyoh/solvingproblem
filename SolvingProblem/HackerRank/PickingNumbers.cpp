#include <vector>
#include <algorithm>

using namespace std;

/*
  Link problem : https://www.hackerrank.com/challenges/picking-numbers/problem
*/

int pickingNumbers(vector<int>& list)
{
  std::sort(begin(list), end(list));
  int indexRef = 0;
  auto lenght = 0;

  for (int i = 0; i < list.size(); )
  {
	auto changeIndex = indexRef;
	while (i < list.size())
	{
	  if (list[i] > list[indexRef] + 1)
		  break;

	  if (changeIndex == indexRef && (list[i] != list[indexRef]))
		  changeIndex = i;
	  i++;
	}

	lenght = std::max(lenght, i - indexRef);
    if (i == list.size())
      break;
	if (list[i] + 1 == list[changeIndex])
	  indexRef = changeIndex;
	else
	  indexRef = i;
  }

  return lenght;
}