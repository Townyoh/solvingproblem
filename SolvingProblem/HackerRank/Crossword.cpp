#include <vector>
#include <string>
#include <utility>
#include <sstream>
#include <iso646.h>
#include <algorithm>
#include <cctype>

using namespace std;

/*
  Link problem : https://www.hackerrank.com/challenges/crossword-puzzle/problem
*/

class CrosswordSolver
{
  vector<pair<int, int>> wordAddress;
  vector<string> crossword;
  vector<vector<pair<string, bool>>> dictionaryWord;

public:
  CrosswordSolver(const vector<string>& original, const string& words)
  {
    crossword = original;
    dictionaryWord = vector<vector<pair<string, bool>>>(11);
    buildDictionaryWord(words);
  }

  void solveCrossword()
  {
    this->findAllBeginWord();
    this->resolve(0);
  }

  vector<string> getResult()
  {
    return crossword;
  }

private:
  void writeWord(int row, int col, int offsetRow, int offsetCol, const string& word)
  {
    for (int index = 0; index < word.size(); index++)
    {
      this->crossword[row][col] = word[index];
      row += offsetRow;
      col += offsetCol;
    }
  }

  bool canDeleteLetter(int row, int col, int offsetRow, int offsetCol)
  {
    if (offsetRow == 1)
    {
      if ((col > 0 && std::isupper(this->crossword[row][col - 1]))
        || (col < this->crossword[row].size() - 1 && std::isupper(this->crossword[row][col + 1])))
      {
        return false;
      }
    }
    else
    {
      if ((row > 0 && std::isupper(this->crossword[row - 1][col]))
        || (row < this->crossword.size() - 1 && std::isupper(this->crossword[row + 1][col])))
      {
        return false;
      }
    }

    return true;
  }

  void deleteWord(int row, int col, int offsetRow, int offsetCol)
  {
    while (row < this->crossword.size() && col < this->crossword[row].size() && (this->crossword[row][col] != '+' && this->crossword[row][col] != 'X'))
    {
      if (this->canDeleteLetter(row, col, offsetRow, offsetCol))
        this->crossword[row][col] = '-';
      row += offsetRow;
      col += offsetCol;
    }
  }

  bool compareWord(const string& ref, const string& potential)
  {
    for (int index = 0; index < ref.size(); index++)
    {
      if (std::isupper(ref[index]) && ref[index] != potential[index])
        return false;
    }

    return true;
  }

  void buildRefWord(int row, int col, int offsetRow, int offsetCol, string& ref)
  {
    while (row < this->crossword.size() && col < this->crossword[row].size() && (this->crossword[row][col] != '+' && this->crossword[row][col] != 'X'))
    {
      ref.push_back(this->crossword[row][col]);
      row += offsetRow;
      col += offsetCol;
    }
  }

  bool resolve(int index)
  {
    if (index >= this->wordAddress.size())
      return true;

    auto wordAddress = this->wordAddress[index];
    auto isHorizontal = this->isHorizontalWord(wordAddress.first, wordAddress.second);
    auto ref = string{};

    if (isHorizontal)
      this->buildRefWord(wordAddress.first, wordAddress.second, 0, 1, ref);
    else
      this->buildRefWord(wordAddress.first, wordAddress.second, 1, 0, ref);

    auto doWeFindCorrectWord = false;

    for (auto& potentialWord : this->dictionaryWord[ref.size()])
    {
      if (not potentialWord.second && this->compareWord(ref, potentialWord.first))
      {
        potentialWord.second = true;
        if (isHorizontal)
          this->writeWord(wordAddress.first, wordAddress.second, 0, 1, potentialWord.first);
        else
          this->writeWord(wordAddress.first, wordAddress.second, 1, 0, potentialWord.first);

        if (this->resolve(index + 1))
        {
          doWeFindCorrectWord = true;
          break;
        }
        else
        {
          doWeFindCorrectWord = false;
          potentialWord.second = false;
          if (isHorizontal)
            this->deleteWord(wordAddress.first, wordAddress.second, 0, 1);
          else
            this->deleteWord(wordAddress.first, wordAddress.second, 1, 0);
        }
      }
    }

    return doWeFindCorrectWord;
  }

  void buildDictionaryWord(const string& words)
  {
    stringstream stream(words);
    auto word = std::string{};

    while (getline(stream, word, ';'))
    {
      word.erase(remove(word.begin(), word.end(), ' '), word.end());
      auto wordSize = word.size();
      this->dictionaryWord[wordSize].push_back(make_pair(word, false));
    }
  }

  bool isHorizontalWord(int row, int col)
  {
    if (col < this->crossword[row].size() - 1 && this->crossword[row][col + 1] == '-')
      return true;

    return false;
  }

  pair<int, int> findStartWord(int row, int col, int offsetRow, int offsetCol, vector<vector<bool>>& alreadyVisited)
  {
    while (row >= 0 && col >= 0 && this->crossword[row][col] == '-')
    {
      row -= offsetRow;
      col -= offsetCol;
    }

    row += offsetRow;
    col += offsetCol;

    return { row, col };
  }

  void tagNewWord(int row, int col, int offsetRow, int offsetCol, vector<vector<bool>>& alreadyVisited)
  {
    while (row < alreadyVisited.size() && col < alreadyVisited[row].size() && this->crossword[row][col] == '-')
    {
      alreadyVisited[row][col] = true;
      row += offsetRow;
      col += offsetCol;
    }
  }

  void findWord(int row, int col, int offsetRow, int offsetCol, vector<vector<bool>>& alreadyVisited)
  {
    auto coorStartWord = this->findStartWord(row, col, offsetRow, offsetCol, alreadyVisited);
    this->tagNewWord(coorStartWord.first, coorStartWord.second, offsetRow, offsetCol, alreadyVisited);

    this->wordAddress.push_back(coorStartWord);
  }

  void findAllBeginWord()
  {
    vector<vector<bool>> alreadyVisited(10, vector<bool>(10, false));

    for (int row = 0; row < this->crossword.size(); row++)
    {
      for (int col = 0; col < this->crossword[row].size(); col++)
      {
        if (this->crossword[row][col] == '-' && not alreadyVisited[row][col])
        {
          bool isHorizontal = this->isHorizontalWord(row, col);
          if (isHorizontal)
            this->findWord(row, col, 0, 1, alreadyVisited);
          else
            this->findWord(row, col, 1, 0, alreadyVisited);
        }
      }
    }
  }
};

vector<string> crosswordPuzzle(vector<string>& crossword, string& words)
{
  CrosswordSolver solver(crossword, words);
  solver.solveCrossword();

  return solver.getResult();
}