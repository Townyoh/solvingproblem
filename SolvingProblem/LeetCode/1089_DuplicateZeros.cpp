#include <vector>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/duplicate-zeros/description/
*/

namespace DuplicateZeros
{
  class Solution 
  {
  public:
    void duplicateZeros(vector<int>& arr) 
    {
      auto temp = vector<int>();

      for (const auto& elem : arr)
      {
        if (temp.size() == arr.size())
          break;
        temp.push_back(elem);
        if (elem == 0 && temp.size() < arr.size())
          temp.push_back(elem);
      }

      std::swap(arr, temp);
    }
  };
}