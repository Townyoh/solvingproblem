#include <vector>
#include <algorithm>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/uncrossed-lines/description/
*/

namespace UncrossedLines
{
  class Solution
  {
  public:
    int maxUncrossedLines(vector<int>& first, vector<int>& second)
    {
      auto maxRow = first.size() + 1;
      auto maxCol = second.size() + 1;
      vector<vector<int>> data(maxRow, vector<int>(maxCol, 0));

      for (int row = 1; row < maxRow; row++)
      {
        for (int col = 1; col < maxCol; col++)
        {
          if (second[col - 1] == first[row - 1])
            data[row][col] = data[row - 1][col - 1] + 1;
          else
            data[row][col] = std::max(data[row][col - 1], data[row - 1][col]);
        }
      }

      return data[maxRow - 1][maxCol - 1];
    }
  };
};

