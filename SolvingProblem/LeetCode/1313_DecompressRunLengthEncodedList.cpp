#include <vector>

using namespace std;

/*
Description:

We are given a list nums of integers representing a list compressed with run-length encoding.

Consider each adjacent pair of elements [a, b] = [nums[2*i], nums[2*i+1]] (with i >= 0).  
For each such pair, there are a elements with value b in the decompressed list.

Return the decompressed list.
*/

/*
  Link problem : https://leetcode.com/problems/decompress-run-length-encoded-list/description/
*/

namespace DecompressRunLengthEncodedList
{
  class Solution
  {
  public:
    vector<int> decompressRLElist(vector<int>& nums)
    {
      auto result = vector<int>();

      for (auto i = 0; i < nums.size(); i += 2)
      {
        for (auto freq = nums[i]; freq > 0; freq--)
          result.push_back(nums[i + 1]);
      }

      return result;
    }
  };
}