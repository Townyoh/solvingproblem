#include <vector>

using namespace std;

/*
Description : 

Say you have an array for which the ith element is the price of a given stock on day i.

Design an algorithm to find the maximum profit. 
You may complete as many transactions as you like (i.e., buy one and sell one share of the stock multiple times).

Note: You may not engage in multiple transactions at the same time (i.e., you must sell the stock before you buy again).
*/

/*
  Link problem : https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/description/
*/

namespace BestTimeToBuyAndSellStockII
{
  class Solution 
  {
  public:
    int maxProfit(vector<int>& prices) 
    {
      auto totalProfit = 0;

      for (auto index = 1; index < prices.size(); index++)
      {
        auto profit = prices[index] - prices[index - 1];

        if (profit > 0)
          totalProfit += profit;
      }

      return totalProfit;
    }
  };
}