#include <vector>
#include <algorithm>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/jump-game-ii/description/
*/

namespace JumpGameII
{
  class Solution
  {
  public:
    int jump(vector<int>& nums)
    {
      if (nums.size() < 2)
        return 0;

      auto layer = 0;
      auto indexNextLayer = nums[0];
      auto indexMaxSoFar = nums[0];

      for (auto index = 1; index < nums.size(); index++)
      {
        indexMaxSoFar = std::max(indexMaxSoFar, index + nums[index]);

        if (index == indexNextLayer)
        {
          layer++;
          if (indexNextLayer == nums.size() - 1)
            break;

          indexNextLayer = indexMaxSoFar;
        }
      }

      layer += indexNextLayer >= nums.size() ? 1 : 0;

      return layer;
    }
  };
}