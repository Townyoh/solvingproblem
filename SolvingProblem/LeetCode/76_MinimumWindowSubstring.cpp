#include <vector>
#include <queue>
#include <cctype>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/minimum-window-substring/description/
*/

/*
Description:

Given a string S and a string T, find the minimum window in S which will contain all the characters in T in complexity O(n).
*/

// #ugly not satisfied of this solution...

namespace MinimumWindowSubstring
{
  class Solution 
  {
  public:
    string minWindow(string& reference, string& word)
    {
      auto result = string("");
      auto countingLetter = vector<int>(128, 0);
      auto isPresentLetter = vector<bool>(128, false);
      auto leftIndex = queue<int>();

      this->buildData(word, countingLetter, isPresentLetter);

      auto right = 0; 
      auto haveAllLetter = this->initRightPosition(reference, word.size(), right, countingLetter, isPresentLetter, leftIndex);

      if (!haveAllLetter)
        return result;

      int currentLeftLetterIndex = leftIndex.empty() ? 0 : reference[leftIndex.front()];

      while (right < reference.size() || countingLetter[currentLeftLetterIndex] <= 0)
      {
        auto left = leftIndex.front();
        leftIndex.pop();

        auto tempResult = reference.substr(left, right - left);
        if (result.empty() || tempResult.size() < result.size())
          std::swap(result, tempResult);

        if (result.size() == word.size())
        {
          haveAllLetter = false;
          break;
        }

        int currentLeftLetterIndex = reference[left];
        countingLetter[currentLeftLetterIndex] += 1;

        if (countingLetter[currentLeftLetterIndex] > 0)
          haveAllLetter = this->findNextMissingLetter(reference, reference[left], right, countingLetter, isPresentLetter, leftIndex);

        if (!haveAllLetter)
          break;
      }

      return result;
    }

  private:
    int letterToIndex(char letter)
    {
      auto index = 0;

      if (std::isupper(letter))
        index = letter - 'A';
      else
        index = letter - 'a';

      return index;
    }

    bool findNextMissingLetter(const string& reference, char letter, int& index, vector<int>& countingLetter, const vector<bool>& isPresentLetter, queue<int>& leftIndex)
    {
      for ( ; index < reference.size(); index++)
      {
        int indexLetter = reference[index];
        if (isPresentLetter[indexLetter])
        {
          leftIndex.push(index);
          countingLetter[indexLetter]--;
        }

        if (reference[index] == letter)
        {
          index++;
          return true;
        }
      }

      return false;
    }

    bool initRightPosition(const string& reference, int size, int& index, vector<int>& countingLetter, const vector<bool>& isPresentLetter, queue<int>& leftIndex)
    {
      auto count = 0;

      for ( ; index < reference.size(); index++)
      {
        int indexLetter = reference[index];
        if (isPresentLetter[indexLetter])
        {
          leftIndex.push(index);
          countingLetter[indexLetter]--;

          count += countingLetter[indexLetter] >= 0 ? 1 : 0;
        }

        if (count == size)
        {
          index++;
          return true;
        }
      }

      return false;
    }

    void buildData(const string& word, vector<int>& countingLetter, vector<bool>& isPresentLetter)
    {
      for (const auto& letter : word)
      {
        int index = letter;

        countingLetter[index] += 1;
        isPresentLetter[index] = true;
      }
    }
  };
}