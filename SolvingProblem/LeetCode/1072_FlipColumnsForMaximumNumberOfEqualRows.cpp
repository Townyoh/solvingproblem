#include <vector>
#include <unordered_map>
#include <sstream>
#include <algorithm>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/flip-columns-for-maximum-number-of-equal-rows/description/
*/

namespace FlipColumnsForMaximumNumberOfEqualRows
{
  class Solution 
  {
  public:
    int maxEqualRowsAfterFlips(vector<vector<int>>& matrix) 
    {
      unordered_map<string, int> data(matrix.size());
      auto result = 0;

      for (auto row = 0; row < matrix.size(); row++)
      {
        auto originalStream = ostringstream();
        auto invertStream = ostringstream();

        for (auto col = 0; col < matrix[row].size(); col++)
        {
          originalStream << matrix[row][col];
          invertStream << !matrix[row][col];
        }

        data[originalStream.str()]++;
        result = std::max(result, data[originalStream.str()]);
        data[invertStream.str()]++;
        result = std::max(result, data[invertStream.str()]);
      }

      return result;
    }
  };
}