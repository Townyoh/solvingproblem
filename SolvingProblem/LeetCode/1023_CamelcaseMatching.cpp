#include <vector>
#include <cctype>
#include <iso646.h>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/camelcase-matching/description/
*/

namespace CamelcaseMatching
{
  class Solution
  {
  public:
    vector<bool> camelMatch(vector<string>& queries, string pattern)
    {
      vector<bool> queriesResult;

      for (const auto& query : queries)
      {
        queriesResult.push_back(this->isQueryMatchPattern(query, pattern));
      }

      return queriesResult;
    }

  private:
    bool isQueryMatchPattern(const string& query, const string& pattern)
    {
      auto iQuery = 0;
      auto iPattern = 0;

      while (iPattern < pattern.size())
      {
        if (std::isupper(pattern[iPattern]))
        {
          if (not this->findUpperInQuery(query, iQuery, pattern[iPattern]))
            return false;
        }
        else
        {
          if (not this->findLowerInQuery(query, iQuery, pattern[iPattern]))
            return false;
        }

        iPattern++;
      }

      this->findUpperInQuery(query, iQuery, 'a');

      return iQuery >= query.size() ? true : false;
    }

    bool findLowerInQuery(const string& query, int& iQuery, char patternLetter)
    {
      while (iQuery < query.size())
      {
        if (std::isupper(query[iQuery]))
        {
          return false;
        }

        if (query[iQuery] == patternLetter)
        {
          iQuery++;
          return true;
        }

        iQuery++;
      }

      return false;
    }

    bool findUpperInQuery(const string& query, int& iQuery, char patternLetter)
    {
      while (iQuery < query.size())
      {
        if (std::isupper(query[iQuery]))
        {
          if (query[iQuery] == patternLetter)
          {
            iQuery++;
            return true;
          }
          else
            return false;
        }

        iQuery++;
      }

      return false;
    }
  };
};

