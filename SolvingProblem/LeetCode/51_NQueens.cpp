#include <vector>
#include <unordered_set>
#include <iso646.h>

using namespace std;

/*
Description:

Given an integer n, return all distinct solutions to the n-queens puzzle.

Each solution contains a distinct board configuration of the n-queens' placement, 
where 'Q' and '.' both indicate a queen and an empty space respectively.
*/

/*
  Link problem : https://leetcode.com/problems/n-queens/description/
*/

namespace NQueens
{
  class Solution
  {
    int size;
    const char queen = 'Q';
    const char empty = '.';
    unordered_set<int> queenInCol;

  public:
    vector<vector<string>> solveNQueens(int size) 
    {
      this->size = size;
      auto grid = vector<string>(size, string(size, this->empty));
      auto result = vector<vector<string>>();

      this->backtracking(result, grid, 0, 0, 0);

      return result;
    }

  private:
    void backtracking(vector<vector<string>>& result, vector<string>& grid, int row, int col, int count)
    {
      if (count == this->size)
      {
        result.push_back(grid);
        return;
      }

      if (row >= this->size)
        return;

      for (; col < this->size; col++)
      {
        if (this->canQueenGoHere(grid, row, col))
        {
          grid[row][col] = this->queen;
          this->queenInCol.insert(col);

          this->backtracking(result, grid, row + 1, 0, count + 1);

          grid[row][col] = this->empty;
          this->queenInCol.erase(col);
        }
      }
    }

    bool canQueenGoHere(vector<string>& grid, int row, int col)
    {
      if (not this->checkCol(col))
        return false;

      if (not this->checkDiagonals(grid, row, col))
        return false;

      return true;
    }

    bool checkDiagonals(vector<string>& grid, int row, int col)
    {
      auto moveRow = row;
      auto moveCol = col;

      while (moveRow >= 0 && moveCol >= 0)
      {
        if (grid[moveRow][moveCol] == this->queen)
          return false;

        moveRow--;
        moveCol--;
      }

      moveRow = row;
      moveCol = col;

      while (moveRow >= 0 && moveCol < this->size)
      {
        if (grid[moveRow][moveCol] == this->queen)
          return false;

        moveRow--;
        moveCol++;
      }

      return true;
    }

    bool checkCol(int col)
    {
      auto elem = this->queenInCol.find(col);

      return elem == this->queenInCol.end();
    }
  };
}