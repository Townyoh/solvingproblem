#include <vector>
#include <queue>
#include <iso646.h>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/matrix-cells-in-distance-order/description/
*/

namespace MatrixCellsInDistanceOrder
{
  class Solution
  {
    vector<vector<int>> result;
    queue<pair<int, int>> fifo;
    vector<int> directionRow{ -1, 0, 1, 0 };
    vector<int> directionCol{ 0, 1, 0, -1 };

  public:

    vector<vector<int>> allCellsDistOrder(int maxRow, int maxCol, int r0, int c0)
    {
      vector<vector<bool>> alreadyVisited(maxRow, vector<bool>(maxCol, false));
      this->addNewCellToVisit(alreadyVisited, r0, c0);

      while (not this->fifo.empty())
      {
        auto elem = fifo.front();
        fifo.pop();
        visitAllNeighboorCell(alreadyVisited, elem.first, elem.second);
      }

      return result;
    }

  private:
    void checkNeighboorCell(vector<vector<bool>>& alreadyVisited, int row, int col)
    {
      if (row < 0 || row >= alreadyVisited.size() || col < 0 || col >= alreadyVisited[row].size())
        return;

      if (alreadyVisited[row][col])
        return;

      this->addNewCellToVisit(alreadyVisited, row, col);
    }

    void visitAllNeighboorCell(vector<vector<bool>>& alreadyVisited, int row, int col)
    {
      for (int direction = 0; direction < 4; direction++)
      {
        checkNeighboorCell(alreadyVisited, row + this->directionRow[direction], col + this->directionCol[direction]);
      }
    }

    void addNewCellToVisit(vector<vector<bool>>& alreadyVisited, int row, int col)
    {
      this->fifo.push(make_pair(row, col));
      alreadyVisited[row][col] = true;
      this->result.push_back({ row, col });
    }
  };
};

