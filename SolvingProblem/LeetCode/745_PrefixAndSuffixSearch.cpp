#include <vector>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/prefix-and-suffix-search/description/
*/

namespace PrefixAndSuffixSearch
{
  // Class defined by exercice
  class WordFilter 
  {
    struct Trie
    {
      vector<Trie*> childs;
      int weight;

      Trie(int _weight = -1)
      {
        childs = vector<Trie*>(27, nullptr);
        weight = _weight;
      }
    };

    Trie* root;

  public:
    WordFilter(vector<string>& words) 
    {
      root = new Trie();

      for (auto index = 0; index < words.size(); index++)
      {
        auto word = words[index] + '{' + words[index];

        for (auto start = 0; start <= words[index].size(); start++)
        {
          auto current = root;
          for (auto i = start; i < word.size(); i++)
          {
            auto letterValue = word[i] - 'a';
            if (current->childs[letterValue] == nullptr)
              current->childs[letterValue] = new Trie(index);

            current = current->childs[letterValue];
            current->weight = index;
          }
        }
      }
    }

    int f(string prefix, string suffix) 
    {
      auto current = this->root;
      auto word = suffix + '{' + prefix;

      for (auto index = 0; index < word.size(); index++)
      {
        auto letterValue = word[index] - 'a';

        if (current->childs[letterValue] == nullptr)
          return -1;

        current = current->childs[letterValue];
      }

      return current->weight;
    }
  };

  /**
   * Your WordFilter object will be instantiated and called as such:
   * WordFilter* obj = new WordFilter(words);
   * int param_1 = obj->f(prefix,suffix);
   */
}