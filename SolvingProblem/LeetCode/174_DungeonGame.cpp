#include <vector>
#include <algorithm>

using namespace std;

/*
Description :

The demons had captured the princess (P) and imprisoned her in the bottom-right corner of a dungeon. 
The dungeon consists of M x N rooms laid out in a 2D grid. Our valiant knight (K) was initially positioned in the top-left room and 
must fight his way through the dungeon to rescue the princess.

The knight has an initial health point represented by a positive integer. 
If at any point his health point drops to 0 or below, he dies immediately.

Some of the rooms are guarded by demons, so the knight loses health (negative integers) upon entering these rooms; 
other rooms are either empty (0's) or contain magic orbs that increase the knight's health (positive integers).

In order to reach the princess as quickly as possible, the knight decides to move only rightward or downward in each step.

Write a function to determine the knight's minimum initial health so that he is able to rescue the princess.
*/

/*
  Link problem : https://leetcode.com/problems/dungeon-game/description/
*/

namespace DungeonGame
{
  class Solution 
  {
    int rowMax;
    int colMax;

  public:
    int calculateMinimumHP(vector<vector<int>>& dungeon) 
    {
      this->rowMax = dungeon.size();
      this->colMax = dungeon[0].size();
      auto minimumHealthByCell = vector<vector<int>>(this->rowMax, vector<int>(this->colMax, 0));

      for (auto row = this->rowMax - 1; row >= 0; row--)
      {
        for (auto col = this->colMax - 1; col >= 0; col--)
        {
          auto minHealth = this->getMinHealthFromCell(minimumHealthByCell, row, col);

          minHealth -= dungeon[row][col];

          if (minHealth <= 0)
            minHealth = 1;

          minimumHealthByCell[row][col] = minHealth;
        }
      }

      return minimumHealthByCell[0][0];
    }

  private:
    int getMinHealthFromCell(const vector<vector<int>>& minimumHealthByCell, int row, int col)
    {
      if (row == this->rowMax - 1 && col == this->colMax - 1)
        return 1;
      else if (row == this->rowMax - 1)
        return minimumHealthByCell[row][col + 1];
      else if (col == this->colMax - 1)
        return minimumHealthByCell[row + 1][col];

      return std::min(minimumHealthByCell[row][col + 1], minimumHealthByCell[row + 1][col]);
    }
  };
}