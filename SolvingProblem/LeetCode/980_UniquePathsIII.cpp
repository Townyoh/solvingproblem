#include <vector>
#include <iso646.h>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/unique-paths-iii/description/
*/

namespace UniquePathsIII
{
  class Solution 
  {
    int startRow;
    int startCol;
    int endRow;
    int endCol;
    int nbCell;
    int nbPath;
    vector<int> dRow{ -1, 0, 1, 0};
    vector<int> dCol{ 0, 1, 0, -1};

  public:
    int uniquePathsIII(vector<vector<int>>& grid)
    {
      this->nbPath = 0;
      this->nbCell = 1;
      this->preProcess(grid);

      auto cellVisited = 0;
      this->dfs(grid, this->startRow, this->startCol, cellVisited);

      return this->nbPath;
    }

  private:
    bool checkLocation(const vector<vector<int>>& grid, int row, int col)
    {
      if (row < 0 || row >= grid.size() || col < 0 || col >= grid[row].size())
        return false;

      if (grid[row][col] < 0)
        return false;

      return true;
    }

    void dfs(vector<vector<int>>& grid, int row, int col, int& cellVisited)
    {
      if (not this->checkLocation(grid, row, col))
        return;

      if (grid[row][col] == 2)
      {
        if (cellVisited == this->nbCell)
          this->nbPath++;
        return;
      }

      grid[row][col] = -2;
      

      for (auto direction = 0; direction < this->dRow.size(); direction++)
      {
        auto nextRow = row + this->dRow[direction];
        auto nextCol = col + this->dCol[direction];

        cellVisited++;
        this->dfs(grid, nextRow, nextCol, cellVisited);
        cellVisited--;
      }

      grid[row][col] = 0;
      
    }

    void preProcess(const vector<vector<int>>& grid)
    {
      for (auto row = 0; row < grid.size(); row++)
      {
        for (auto col = 0; col < grid[row].size(); col++)
        {
          if (grid[row][col] == 1)
          {
            this->startRow = row;
            this->startCol = col;
          }
          else if (grid[row][col] == 2)
          {
            this->endRow = row;
            this->endCol = col;
          }
          else if (grid[row][col] == 0)
            this->nbCell++;
        }
      }
    }
  };
}