#include <vector>
#include <unordered_map>
#include <iso646.h>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/word-search-ii/description/
*/

namespace WordSearchII
{
  class Solution
  {
    vector<int> dRow{ -1, 0, 1, 0 };
    vector<int> dCol{ 0, 1, 0, -1 };

    struct Node
    {
      unordered_map<int, Node*> childs;
      string word;

      Node()
      {
        childs = unordered_map<int, Node*>();
        word = string();
      }

      Node(int value)
      {
        word = string();
        childs.insert(make_pair(value, nullptr));
      }
    };

  public:
    vector<string> findWords(vector<vector<char>>& board, vector<string>& words)
    {
      auto root = new Node();
      this->buildTrie(words, root);
      auto result = vector<string>();

      for (auto row = 0; row < board.size(); row++)
      {
        for (auto col = 0; col < board[row].size(); col++)
        {
          this->dfs(board, row, col, root, result);
        }
      }

      return result;
    }

  private:
    bool dfs(vector<vector<char>>& board, int currentRow, int currentCol, Node* currentNode, vector<string>& result)
    {
      if (currentNode->childs.empty())
        return false;

      auto letter = board[currentRow][currentCol] - 'a';
      auto child = currentNode->childs.find(letter);

      if (child == currentNode->childs.end())
        return false;

      auto nextNode = child->second;

      if (not nextNode->word.empty())
      {
        result.push_back(nextNode->word);
        nextNode->word = "";

        if (nextNode->childs.empty())
          return true;

      }

      board[currentRow][currentCol] = '-';

      for (auto direction = 0; direction < this->dCol.size(); direction++)
      {
        auto nextRow = currentRow + this->dRow[direction];
        auto nextCol = currentCol + this->dCol[direction];

        if (this->checkNeigh(nextRow, nextCol, board))
        {
          if (this->dfs(board, nextRow, nextCol, nextNode, result))
          {
            currentNode->childs[letter]->childs.erase(board[nextRow][nextCol] - 'a');
          }
        }
      }

      board[currentRow][currentCol] = letter + 'a';

      return currentNode->childs.empty();
    }

    bool checkNeigh(int nextRow, int nextCol, const vector<vector<char>>& board)
    {
      if (nextRow < 0 || nextCol < 0 || nextRow >= board.size() || nextCol >= board[nextRow].size())
        return false;

      return true;
    }

    void buildTrie(const vector<string>& words, Node* root)
    {
      for (const auto& word : words)
      {
        auto current = root;
        for (const auto& letter : word)
        {
          auto value = letter - 'a';

          auto elem = current->childs.insert(make_pair(value, new Node()));
          current = elem.first->second;
        }

        current->word = word;
      }
    }
  };

  /*class Solution 
  {
    vector<int> dRow{ -1, 0, 1, 0 };
    vector<int> dCol{ 0, 1, 0, -1 };

  public:
    vector<string> findWords(vector<vector<char>>& board, vector<string>& words)
    {
      auto data = vector<vector<int>>(26, vector<int>());
      this->preProcess(board, data);
      auto result = vector<string>();

      for (const auto& word : words)
      {
        auto firstLetter = word[0] - 'a';

        for (const auto& coor : data[firstLetter])
        {
          auto row = coor / board[0].size();
          auto col = coor % board[0].size();

          if (this->dfs(word, 0, row, col, board))
          {
            result.push_back(word);
            break;
          }
        }
      }

      return result;
    }

  private:
    bool checkNeigh(const char nextLetter, int nextRow, int nextCol, const vector<vector<char>>& board)
    {
      if (nextRow < 0 || nextCol < 0 || nextRow >= board.size() || nextCol >= board[nextRow].size())
        return false;

      if (board[nextRow][nextCol] != nextLetter)
        return false;

      return true;
    }

    bool dfs(const string& word, int index, int currentRow, int currentCol, vector<vector<char>>& board)
    {
      if (index == word.size() - 1)
        return true;

      board[currentRow][currentCol] = '-';
 
      for (auto direction = 0; direction < this->dCol.size(); direction++)
      {
        auto nextRow = currentRow + this->dRow[direction];
        auto nextCol = currentCol + this->dCol[direction];

        if (this->checkNeigh(word[index + 1], nextRow, nextCol, board))
        {
          if (this->dfs(word, index + 1, nextRow, nextCol, board))
          {
            board[currentRow][currentCol] = word[index];
            return true;
          }
        }
      }

      board[currentRow][currentCol] = word[index];

      return false;
    }

    void preProcess(const vector<vector<char>>& board, vector<vector<int>>& data)
    {
      for (auto row = 0; row < board.size(); row++)
      {
        for (auto col = 0; col < board[row].size(); col++)
        {
          auto index = board[row][col] - 'a';
          data[index].push_back(col + (board[row].size() * row));
        }
      }
    }
  };*/
}