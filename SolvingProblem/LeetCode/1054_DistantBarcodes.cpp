#include <vector>
#include <queue>
#include <algorithm>
#include <unordered_map>
#include <iso646.h>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/distant-barcodes/description/
*/

namespace DistantBarcodes
{
  class Solution 
  {
    struct Barcode
    {
      int value;
      int counter;

      Barcode(int v, int c)
        : value(v), counter(c)
      {}
    };

    struct Compare
    {
      bool operator()(const Barcode& first, const Barcode& second)
      {
        return first.counter < second.counter;
      }
    };

  public:
    vector<int> rearrangeBarcodes(vector<int>& barcodes) 
    {
      priority_queue<Barcode, vector<Barcode>, Compare> priority;
      auto result = vector<int>();
      this->buildPriority(priority, barcodes);

      while (not priority.empty())
      {
        auto bar = priority.top();
        priority.pop();
        if (result.empty() || result.back() != bar.value)
          this->addValueToResult(priority, result, bar);
        else
        {
          auto nextBar = priority.top();
          priority.pop();

          this->addValueToResult(priority, result, nextBar);

          priority.push(bar);
        }
      }

      return result;
    }

  private:
    void addValueToResult(priority_queue<Barcode, vector<Barcode>, Compare>& priority,
      vector<int>& result, Barcode& bar)
    {
      result.push_back(bar.value);
      bar.counter--;
      if (bar.counter > 0)
        priority.push(bar);
    }

    void buildPriority(priority_queue<Barcode, vector<Barcode>, Compare>& priority, vector<int>& barcodes)
    {
      unordered_map<int, int> data;

      for (const auto& barcode : barcodes)
      {
        auto elem = data.insert(make_pair(barcode, 1));
        if (not elem.second)
          elem.first->second++;
      }

      for (const auto& elem : data)
      {
        priority.push(Barcode(elem.first, elem.second));
      }
    }
  };
}