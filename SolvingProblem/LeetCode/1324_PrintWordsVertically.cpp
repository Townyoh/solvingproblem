#include <vector>
#include <cctype>

using namespace std;

/*
Description:

Given a string s. Return all the words vertically in the same order in which they appear in s.
Words are returned as a list of strings, complete with spaces when is necessary. (Trailing spaces are not allowed).
Each word would be put on only one column and that in one column there will be only one word.
*/

/*
  Link problem : https://leetcode.com/problems/print-words-vertically/description/
*/

namespace PrintWordsVertically
{
  class Solution
  {
  public:
    vector<string> printVertically(string str)
    {
      auto result = vector<string>();
      auto nbWord = 0;

      for (auto i = 0; i < str.size(); i++)
      {
        this->processWord(str, i, result, nbWord);
        nbWord++;
      }

      return result;
    }

  private:
    void processWord(const string& str, int& i, vector<string>& result, int nbWord)
    {
      for (auto count = 0; std::isalpha(str[i]); i++, count++)
      {
        if (count >= result.size())
          result.push_back("");

        this->addSpace(result, nbWord, count);
        result[count].push_back(str[i]);
      }
    }

    void addSpace(vector<string>& result, int nbLetter, int index)
    {
      auto nbSpace = nbLetter - result[index].size();
      for (; nbSpace > 0; nbSpace--)
        result[index].push_back(' ');
    }
  };
}