#include <vector>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/first-missing-positive/description/
*/

namespace FirstMissingPositive
{
  class Solution 
  {
  public:
    int firstMissingPositive(vector<int>& nums) 
    {
      for (auto index = 0; index < nums.size(); index++)
      {
        this->sort(nums, index);
      }

      return this->firstMissing(nums);
    }

  private:
    int firstMissing(const vector<int>& nums)
    {
      for (auto index = 0; index < nums.size(); index++)
      {
        if (nums[index] != index + 1)
          return index + 1;
      }

      return nums.size() + 1;
    }

    void sort(vector<int>& nums, int index)
    {
      while (nums[index] != index + 1)
      {
        if (nums[index] <= 0 || nums[index] > nums.size())
          break;

        if (nums[nums[index] - 1] == nums[index])
          break;

        auto temp = nums[nums[index] - 1];
        nums[nums[index] - 1] = nums[index];
        nums[index] = temp;
      }
    }
  };
}