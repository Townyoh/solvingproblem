#include <vector>
#include <unordered_map>
#include <algorithm>

using namespace std;

/*
Description:

Given an array of integers arr, replace each element with its rank.

The rank represents how large the element is. The rank has the following rules:

Rank is an integer starting from 1.
The larger the element, the larger the rank. If two elements are equal, their rank must be the same.
Rank should be as small as possible.
*/

/*
  Link problem : https://leetcode.com/problems/rank-transform-of-an-array/
*/

namespace RankTransformOfAnArray
{
  class Solution
  {
  public:
    vector<int> arrayRankTransform(vector<int>& arr)
    {
      auto temp = arr;
      std::sort(temp.begin(), temp.end());

      auto dict = unordered_map<int, int>();
      this->buildDict(dict, temp);

      auto result = vector<int>();

      for (const auto& elem : arr)
      {
        result.push_back(dict[elem]);
      }

      return result;
    }

  private:
    void buildDict(unordered_map<int, int>& dict, const vector<int>& temp)
    {
      auto counter = 1;

      for (auto index = 0; index < temp.size(); index++)
      {
        auto newElem = dict.insert(make_pair(temp[index], counter));
        if (newElem.second)
          counter++;
      }
    }
  };
}