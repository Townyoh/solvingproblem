#include <string>
#include <stack>
#include <iso646.h>
#include <algorithm>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/longest-valid-parentheses/description/
*/

namespace LongestValidParentheses
{
  class Solution 
  {
  public:
    int longestValidParentheses(string str) 
    {
      auto lifo = stack<int>();
      lifo.push(-1);
      auto longest = 0;

      for (auto index = 0; index < str.size(); index++)
      {
        if (str[index] == '(')
          lifo.push(index);
        else
        {
          lifo.pop();

          if (lifo.empty())
            lifo.push(index);
          else
            longest = std::max(longest, index - lifo.top());
        }
      }

      return longest;
    }
  };
}