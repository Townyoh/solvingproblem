#include <vector>
#include <algorithm>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/video-stitching/description/
*/

namespace VideoStitching
{
  class Solution
  {
  public:
    int videoStitching(vector<vector<int>>& clips, int time)
    {
      this->sort(clips);

      if (clips[0][0] > 0)
        return -1;

      auto counter = 1;
      auto ref = 0;
      for (int index = 0; index < clips.size(); )
      {
        if (clips[ref][1] >= time)
          break;

        auto save = -1;
        auto forward = index + 1;
        while (forward < clips.size() && clips[forward][0] <= clips[ref][1])
        {
          if (save < 0 || clips[forward][1] > clips[save][1])
            save = forward;
          forward++;
        }

        if (save == -1 && forward < clips.size())
          return -1;

        ref = (save == -1) ? index : save;
        counter++;

        index = forward;
      }

      if (clips[ref][1] < time)
        return -1;

      return counter;
    }

  private:
    void sort(vector<vector<int>>& clips)
    {
      std::sort(clips.begin(), clips.end(),
        [](const vector<int>& first, const vector<int>& second)
      {
        if (first[0] < second[0])
          return true;

        if (first[0] == second[0] && first[1] >= second[1])
          return true;

        return false;
      });
    }
  };
};

