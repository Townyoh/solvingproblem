#include <vector>
#include <algorithm>
#include <iso646.h>

using namespace std;

//Definition for singly-linked list given by exercice.
struct ListNode
{
  int val;
  ListNode *next;
  ListNode(int x)
    : val(x), next(nullptr)
  {}
};

/*
  Link problem : https://leetcode.com/problems/merge-k-sorted-lists/description/
*/

namespace MergeKSortedLists
{
  class Solution
  {
    struct Compare
    {
      bool operator()(const ListNode* first, const ListNode* second)
      {
        return first->val > second->val;
      }
    };

  public:
    ListNode* mergeKLists(vector<ListNode*>& lists) 
    {
      if (lists.empty())
        return nullptr;

      lists.erase(std::remove_if(lists.begin(), lists.end(), 
        [](const ListNode* current)
      {
        return current == nullptr;
      }), lists.end());

      std::make_heap(lists.begin(), lists.end(), Compare());
      auto value = 0;
      ListNode* head = nullptr;

      if (this->getNextValue(lists, value))
        head = new ListNode(value);
      auto current = head;

      while (not lists.empty())
      {
        if (this->getNextValue(lists, value))
        {
          current->next = new ListNode(value);
          current = current->next;
        }
      }

      return head;
    }

  private:
    bool getNextValue(vector<ListNode*>& lists, int& value)
    {
      auto node = lists[0];
      std::pop_heap(lists.begin(), lists.end(), Compare());
      lists.pop_back();

      if (node == nullptr)
        return false;

      value = node->val;

      if (node->next != nullptr)
      {
        node = node->next;
        lists.push_back(node);
        std::push_heap(lists.begin(), lists.end(), Compare());
      }

      return true;
    }
  };
}