#include <string>
#include <sstream>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/defanging-an-ip-address/description/
*/

namespace DefangingIpAddress
{
  class Solution 
  {
  public:
    string defangIPaddr(string address) 
    {
      auto stream = std::stringstream();

      for (const auto& letter : address)
      {
        if (letter == '.')
          stream << "[.]";
        else
          stream << letter;
      }

      return stream.str();
    }
  };
}