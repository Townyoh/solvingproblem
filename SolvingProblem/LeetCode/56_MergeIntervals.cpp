#include <vector>
#include <algorithm>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/merge-intervals/description/
*/

namespace MergeIntervals
{
  class Solution
  {
  public:
    vector<vector<int>> merge(vector<vector<int>>& intervals) 
    {
      auto result = vector<vector<int>>();

      std::sort(intervals.begin(), intervals.end(),
        [](const vector<int>& first, const vector<int>& second)
      {
        return first[0] < second[0];
      });

      for (const auto& interval : intervals)
      {
        if (result.empty() || interval[0] > result.back()[1])
          result.push_back(interval);
        else
          result.back()[1] = std::max(result.back()[1], interval[1]);
      }

      return result;
    }
  };
}