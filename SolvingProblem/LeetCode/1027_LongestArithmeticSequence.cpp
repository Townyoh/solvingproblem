#include <vector>
#include <unordered_map>
#include <iso646.h>
#include <algorithm>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/longest-arithmetic-sequence/description/
*/

namespace LongestArithmeticSequence
{
  class Solution
  {
  public:
    int longestArithSeqLength(vector<int>& list)
    {
      auto length = 2;
      // key => difference
      // key => last value of subsequence
      // value => size of this subsequence
      unordered_map<int, unordered_map<int, int>> data;

      for (auto i = 0; i < list.size(); i++)
      {
        for (auto j = i + 1; j < list.size(); j++)
        {
          auto diff = list[i] - list[j];
          unordered_map<int, int> temp{ { j, 2 } };

          auto diffElem = data.insert(make_pair(diff, temp));
          if (not diffElem.second)
          {
            auto lastItem = diffElem.first->second.find(i);
            if (lastItem != std::end(diffElem.first->second))
            {
              auto save = lastItem->second + 1;
              data[diff][j] = save;
              length = std::max(length, save);
            }
            else
              diffElem.first->second.insert(make_pair(j, 2));
          }
        }
      }

      return length;
    }
  };
};

