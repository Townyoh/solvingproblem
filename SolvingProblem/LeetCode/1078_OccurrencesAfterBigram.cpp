#include <vector>
#include <sstream>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/occurrences-after-bigram/description/
*/

namespace OccurrencesAfterBigram
{
  class Solution 
  {
  public:
    vector<string> findOcurrences(string text, string first, string second) 
    {
      auto wordList = vector<string>();
      auto result = vector<string>();

      this->transformStringToVector(text, wordList);

      for (auto index = 0; index < wordList.size(); index++)
      {
        if (wordList[index] == first)
        {
          if (index < wordList.size() - 2 && wordList[index + 1] == second)
          {
            if (index < wordList.size() - 2)
              result.push_back(wordList[index + 2]);
          }
        }
      }

      return result;
    }

  private:

    void transformStringToVector(const string& text, vector<string>& wordList)
    {
      istringstream stream(text);
      auto word = string();

      while (stream >> word)
        wordList.push_back(word);
    }
  };
}