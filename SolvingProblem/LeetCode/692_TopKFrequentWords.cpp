#include <vector>
#include <unordered_map>
#include <iso646.h>
#include <queue>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/top-k-frequent-words/description/
*/

namespace TopKFrequentWords
{
  struct Compare
  {
    bool operator()(const std::pair<string, int>& first, const std::pair<string, int>& second)
    {
      if (first.second == second.second)
        return first.first > second.first;

      return first.second < second.second;
    }
  };

  class Solution 
  {
  public:
    vector<string> topKFrequent(vector<string>& words, int k) 
    {
      auto dict = unordered_map<string, int>(words.size());
      this->buildDict(words, dict);

      std::priority_queue<std::pair<string, int>, vector<pair<string, int>>, Compare> priority;

      for (const auto& elem : dict)
      {
        priority.push(elem);
      }

      auto result = vector<string>();
      for (auto index = 0; index < k; index++)
      {
        if (priority.empty())
          break;
        auto elem = priority.top();
        priority.pop();
        result.push_back(elem.first);
      }

      return result;
    }

  private:
    void buildDict(const vector<string>& words, unordered_map<string, int>& dict)
    {
      for (const auto& word : words)
      {
        auto elem = dict.insert(make_pair(word, 1));

        if (not elem.second)
          elem.first->second++;
      }
    }
  };
}