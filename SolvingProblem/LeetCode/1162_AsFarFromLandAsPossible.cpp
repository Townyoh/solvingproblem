#include <vector>
#include <queue>
#include <iso646.h>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/merge-k-sorted-lists/description/
*/

/*
Description:

Given an N x N grid containing only values 0 and 1, where 0 represents water and 1 represents land, 
find a water cell such that its distance to the nearest land cell is maximized and return the distance.

The distance used in this problem is the Manhattan distance:
the distance between two cells (x0, y0) and (x1, y1) is |x0 - x1| + |y0 - y1|.

If no land or water exists in the grid, return -1.
*/

namespace AsFarFromLandAsPossible
{
  class Solution 
  {
    const vector<int> dRow{ -1, 0, 1, 0 };
    const vector<int> dCol{ 0, 1, 0, -1 };

  public:
    int maxDistance(vector<vector<int>>& grid) 
    {
      auto fifo = queue<pair<int, int>>();
      this->initFifoListByLand(grid, fifo);

      if (fifo.empty())
        return -1;

      return this->bfs(grid, fifo);
    }

  private:
    int bfs(vector<vector<int>>& grid, queue<pair<int, int>>& fifo)
    {
      int layer = 0;

      while (not fifo.empty())
      {
        for (auto size = fifo.size(); size > 0; size--)
        {
          auto coor = fifo.front();
          fifo.pop();

          this->checkNeighboor(grid, fifo, coor.first, coor.second);
        }

        if (layer == 0 && fifo.empty())
          return -1;

        if (not fifo.empty())
          layer++;
      }

      return layer;
    }

    void checkNeighboor(vector<vector<int>>& grid, queue<pair<int, int>>& fifo, int row, int col)
    {
      for (auto direction = 0; direction < this->dRow.size(); direction++)
      {
        auto nextRow = row + this->dRow[direction];
        auto nextCol = col + this->dCol[direction];

        if (this->isValidNeighboor(grid, nextRow, nextCol))
        {
          fifo.push(make_pair(nextRow, nextCol));
          grid[nextRow][nextCol] = -1;
        }
      }
    }

    bool isValidNeighboor(const vector<vector<int>>& grid, int row, int col)
    {
      if (row < 0 || row >= grid.size() || col < 0 || col >= grid[row].size() || grid[row][col] != 0)
        return false;

      return true;
    }

    void initFifoListByLand(vector<vector<int>>& grid, queue<pair<int, int>>& fifo)
    {
      for (auto row = 0; row < grid.size(); row++)
      {
        for (auto col = 0; col < grid[row].size(); col++)
        {
          if (grid[row][col] == 1)
          {
            fifo.push(make_pair(row, col));
            grid[row][col] = -1;
          }
        }
      }
    }
  };
}