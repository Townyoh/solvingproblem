#pragma once

//Definition for a binary tree node given by excercise
struct TreeNode 
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) 
      : val(x), left(nullptr), right(nullptr)
    {}
};
 
/*
  Link problem : https://leetcode.com/problems/binary-search-tree-to-greater-sum-tree/description/
*/

namespace BinarySearchTreeToGreaterSumTree
{
  class Solution
  {
    int addValue;

  public:
    TreeNode* bstToGst(TreeNode* root)
    {
      this->addValue = 0;

      this->postOrderRight(root);

      return root;
    }

  private:
    void postOrderRight(TreeNode* root)
    {
      if (root == nullptr)
        return;

      this->postOrderRight(root->right);

      this->addValue += root->val;
      root->val = this->addValue;

      this->postOrderRight(root->left);
    }
  };
};

