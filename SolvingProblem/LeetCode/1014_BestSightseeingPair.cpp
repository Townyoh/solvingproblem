#include <vector>
#include <algorithm>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/best-sightseeing-pair/description/
*/

/*
Kind of problem that I have to work, difficult to me
*/
namespace BestSightseeingPair
{
  class Solution 
  {
  public:
    int maxScoreSightseeingPair(vector<int>& list)
    {
      auto total = 0;
      auto current = 0;

      for (int index = 0; index < list.size(); index++)
      {
        total = std::max(total, current + list[index] - index);
        current = std::max(current, list[index] + index);
      }

      return total;
    }
  };
}