#include <vector>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/flower-planting-with-no-adjacent/description/
*/

namespace FlowerPlantingWithNoAdjacent
{
  class Solution 
  {
  public:
    vector<int> gardenNoAdj(int size, vector<vector<int>>& paths)
    {
      vector<vector<int>> graph(size);
      vector<int> result(size, 0);

      this->buildGraph(graph, paths);

      for (int node = 0; node < graph.size(); node++)
      {
        auto flowers = vector<bool>(5, false);
        for (const auto& nei : graph[node])
        {
          flowers[result[nei]] = true;
        }
        for (int flowerColor = 4; flowerColor > 0; flowerColor--)
        {
          result[node] = (flowers[flowerColor]) ? result[node] : flowerColor;
        }
      }

      return result;
    }

  private:
    void buildGraph(vector<vector<int>>& graph, const vector<vector<int>>& paths)
    {
      for (const auto& link : paths)
      {
        graph[link[0] - 1].push_back(link[1] - 1);
        graph[link[1] - 1].push_back(link[0] - 1);
      }
    }
  };
};