#include <vector>
#include <algorithm>

using namespace std;

/*
Description :

Given two words word1 and word2, find the minimum number of operations required to convert word1 to word2.

You have the following 3 operations permitted on a word:
  * Insert a character
  * Delete a character
  * Replace a character
*/

/*
  Link problem : https://leetcode.com/problems/edit-distance/description/
*/

namespace EditDistance
{
  class Solution 
  {
  public:
    int minDistance(string word1, string word2) 
    {
      auto data = vector<vector<int>>(word1.size() + 1, vector<int>(word2.size() + 1));
      this->initData(data);

      for (auto row = 1; row <= word1.size(); row++)
      {
        for (auto col = 1; col <= word2.size(); col++)
        {
          if (word1[row - 1] == word2[col - 1])
            data[row][col] = data[row - 1][col - 1];
          else 
            data[row][col] = std::min({data[row - 1][col - 1], data[row - 1][col], data[row][col - 1]}) + 1;
        }
      }

      return data[word1.size()][word2.size()];
    }

  private:
    void initData(vector<vector<int>>& data)
    {
      for (auto index = 0; index < data.size(); index++)
        data[index][0] = index;

      for (auto index = 0; index < data[0].size(); index++)
        data[0][index] = index;
    }
  };

}