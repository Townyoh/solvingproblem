#include <vector>
#include <algorithm>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/longest-increasing-path-in-a-matrix/description/
*/

namespace LongestIncreasingPathInAMatrix
{
  class Solution
  {
    int rowMax;
    int colMax;
    int longest;
    vector<int> dRow = { -1, 0, 1, 0 };
    vector<int> dCol = { 0, 1, 0, -1 };

  public:
    int longestIncreasingPath(vector<vector<int>>& matrix)
    {
      this->longest = 0;
      this->rowMax = matrix.size();

      if (this->rowMax < 1)
        return this->longest;

      this->colMax = matrix[0].size();
      auto data = vector<vector<int>>(this->rowMax, vector<int>(this->colMax, 0));

      for (auto row = 0; row < this->rowMax; row++)
      {
        for (auto col = 0; col < this->colMax; col++)
        {
          this->dfs(matrix, data, row, col);
        }
      }

      return this->longest;
    }

  private:
    bool shouldGoToNeighboor(const vector<vector<int>>& matrix, int currentValue, int nextRow, int nextCol)
    {
      if (nextRow < 0 || nextRow >= this->rowMax || nextCol < 0 || nextCol >= this->colMax)
        return false;

      if (matrix[nextRow][nextCol] >= currentValue)
        return false;

      return true;
    }

    int dfs(const vector<vector<int>>& matrix, vector<vector<int>>& data, int row, int col)
    {
      if (data[row][col] > 0)
        return data[row][col];

      data[row][col] = 1;

      auto deep = 0;

      for (auto direction = 0; direction < this->dRow.size(); direction++)
      {
        auto nextRow = row + this->dRow[direction];
        auto nextCol = col + this->dCol[direction];

        if (this->shouldGoToNeighboor(matrix, matrix[row][col], nextRow, nextCol))
        {
          deep = std::max(deep, this->dfs(matrix, data, nextRow, nextCol));
        }
      }

      data[row][col] += deep;

      this->longest = std::max(this->longest, data[row][col]);

      return data[row][col];
    }
  };
}