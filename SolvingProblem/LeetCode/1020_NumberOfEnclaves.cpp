#include <vector>
#include <queue>
#include <iso646.h>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/number-of-enclaves/description/
*/

namespace NumberOfEnclaves
{
  const vector<int> dRow = { -1, 0, 1, 0 };
  const vector<int> dCol = { 0, 1, 0, -1 };

  class Solution
  {
  public:

    int numEnclaves(vector<vector<int>>& data)
    {
      auto enclosedLand = 0;

      for (int row = 0; row < data.size(); row++)
      {
        for (int col = 0; col < data[row].size(); col++)
        {
          if (data[row][col] == 1)
          {
            enclosedLand += bfs(data, row, col);
          }
        }
      }

      return enclosedLand;
    }

  private:
    int bfs(vector<vector<int>>& data, int row, int col)
    {
      queue<pair<int, int>> fifo;
      this->visitLand(data, fifo, row, col);
      bool boundary = false;
      int nbLand = 0;

      while (not fifo.empty())
      {
        auto position = fifo.front();
        fifo.pop();
        if (this->addConnectedLand(data, fifo, position.first, position.second))
          boundary = true;

        nbLand++;
      }

      return (boundary == true) ? 0 : nbLand;
    }

    bool addConnectedLand(vector<vector<int>>& data, queue<pair<int, int>>& fifo, int row, int col)
    {
      auto boundary = false;

      for (auto direction = 0; direction < dRow.size(); direction++)
      {
        auto tempRow = row + dRow[direction];
        auto tempCol = col + dCol[direction];

        if (checkConnectedLand(data, fifo, tempRow, tempCol))
          boundary = true;
      }

      return boundary;
    }

    bool checkConnectedLand(vector<vector<int>>& data, queue<pair<int, int>>& fifo, int row, int col)
    {
      if (row < 0 || row >= data.size() || col < 0 || col >= data[row].size())
        return true;

      if (data[row][col] == 1)
        this->visitLand(data, fifo, row, col);

      return false;
    }

    void visitLand(vector<vector<int>>& data, queue<pair<int, int>>& fifo, int row, int col)
    {
      fifo.push(make_pair(row, col));
      data[row][col] = 0;
    }
  };

};

