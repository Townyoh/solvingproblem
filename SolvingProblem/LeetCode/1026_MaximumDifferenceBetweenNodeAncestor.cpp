#include <cmath>
#include <algorithm>

//Definition for a binary tree node.
struct TreeNode 
{
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode(int x)
    : val(x), left(nullptr), right(nullptr)
  {}
};

/*
  Link problem : https://leetcode.com/problems/maximum-difference-between-node-and-ancestor/description/
*/

namespace MaximumDifferenceBetweenNodeAncestor
{
  class Solution
  {
    int diff;

  public:
    int maxAncestorDiff(TreeNode* root)
    {
      this->diff = 0;

      if (root != nullptr)
        this->preOrder(root, root->val, root->val);

      return this->diff;
    }

  private:
    void preOrder(TreeNode* node, int min, int max)
    {
      if (node == nullptr)
        return;

      this->diff = std::max(this->diff, std::abs(node->val - min));
      this->diff = std::max(this->diff, std::abs(node->val - max));

      min = std::min(min, node->val);
      max = std::max(max, node->val);

      this->preOrder(node->left, min, max);
      this->preOrder(node->right, min, max);
    }
  };
};

