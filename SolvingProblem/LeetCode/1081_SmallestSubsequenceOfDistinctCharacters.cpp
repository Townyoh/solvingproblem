#include <string>
#include <vector>
#include <unordered_map>
#include <memory>
#include <iso646.h>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/smallest-subsequence-of-distinct-characters/description/
*/

namespace SmallestSubsequenceOfDistinctCharacters
{
  class Solution
  {
  public:
    string smallestSubsequence(string text)
    {
      auto result = string();
      auto totalLetter = vector<int>(26, 0);
      auto inCurrentSolution = vector<int>(26, 0);
      this->countTotalLetter(text, totalLetter);

      for (const auto& letter : text)
      {
        auto valueLetter = letter - 'a';
        totalLetter[valueLetter]--;
        if (inCurrentSolution[valueLetter] == 0)
        {
          while (not result.empty())
          {
            auto backValue = result.back() - 'a';
            if (backValue < valueLetter || totalLetter[backValue] == 0)
              break;

            result.pop_back();
            inCurrentSolution[backValue] = 0;
          }

          inCurrentSolution[valueLetter]++;
          result.push_back(letter);
        }
      }

      return result;
    }

  private:
    void countTotalLetter(const string& text, vector<int>& totalLetter)
    {
      for (const auto& letter : text)
      {
        auto valueLetter = letter - 'a';
        totalLetter[valueLetter]++;
      }
    }
  };

  // Solution cache like work but not totally
  // To simple to works with complexe input
  //class Solution 
  //{
  //  struct Letter
  //  {
  //    int value;
  //    shared_ptr<Letter> next;
  //    shared_ptr<Letter> prev;

  //    Letter(int v)
  //      : value(v), next(nullptr), prev(nullptr)
  //    {}
  //  };

  //public:
  //  string smallestSubsequence(string text) 
  //  {
  //    shared_ptr<Letter> endTail = nullptr;
  //    unordered_map<int, shared_ptr<Letter>> data(26);
  //    auto newValue = 0;

  //    for (const auto& letter : text)
  //    {
  //      auto letterValue = letter - 'a';
  //      auto elem = data.find(letterValue);

  //      if (elem == data.end())
  //      {
  //        this->insertLetterToTail(letterValue, endTail, data);
  //        newValue++;
  //      }
  //      else
  //        this->checkNextValue(letterValue, endTail, data);
  //    }
  //    
  //    auto result = string(newValue--, ' ');
  //    while (endTail != nullptr)
  //    {
  //      result[newValue--] = endTail->value + 'a';
  //      endTail = endTail->prev;
  //    }

  //    return result;
  //  }

  //private:
  //  void checkNextValue(int letterValue, shared_ptr<Letter>& endTail, unordered_map<int, shared_ptr<Letter>>& data)
  //  {
  //    auto nextValue = data[letterValue]->next;
  //    if (nextValue != nullptr && nextValue->value < data[letterValue]->value)
  //    {
  //      nextValue->prev = data[letterValue]->prev;
  //      if (nextValue->prev != nullptr)
  //        nextValue->prev->next = nextValue;

  //      this->insertLetterToTail(letterValue, endTail, data);
  //    }
  //  }

  //  void insertLetterToTail(int letterValue, shared_ptr<Letter>& endTail, unordered_map<int, shared_ptr<Letter>>& data)
  //  {
  //    auto newValue = make_shared<Letter>(letterValue);
  //    if (endTail != nullptr)
  //    {
  //      endTail->next = newValue;
  //      newValue->prev = endTail;
  //    }

  //    endTail.swap(newValue);
  //    data[letterValue] = endTail;
  //  }
  //};
}