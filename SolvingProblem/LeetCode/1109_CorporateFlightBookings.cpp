#include <vector>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/corporate-flight-bookings/description/
*/

namespace CorporateFlightBookings
{
  class Solution 
  {
  public:
    vector<int> corpFlightBookings(vector<vector<int>>& bookings, int nbFlight)
    {
      auto result = vector<int>(nbFlight + 1, 0);

      for (const auto& book : bookings)
      {
        result[book[0] - 1] += book[2];
        result[book[1]] -= book[2];
      }

      for (auto index = 1; index < nbFlight; index++)
      {
        result[index] += result[index - 1];
      }

      result.pop_back();

      return result;
    }
  };
}