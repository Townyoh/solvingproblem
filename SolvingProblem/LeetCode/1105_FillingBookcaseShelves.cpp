#include <vector>
#include <algorithm>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/filling-bookcase-shelves/description/
*/

namespace FillingBookcaseShelves
{
  class Solution 
  {
  public:
    int minHeightShelves(vector<vector<int>>& books, int shelf_width)
    {
      auto dp = vector<int>(books.size() + 1, 0);

      for (int index = 0; index < books.size(); index++)
      {
        auto currentWidth = 0;
        auto currentHeigh = 0;
        dp[index + 1] = dp[index] + books[index][1];

        for (auto bIndex = index; bIndex >= 0; bIndex--)
        {
          currentWidth += books[bIndex][0];

          if (currentWidth > shelf_width)
            break;
          
          currentHeigh = std::max(currentHeigh, books[bIndex][1]);
          dp[index + 1] = std::min(dp[index + 1], currentHeigh + dp[bIndex]);
        }
      }

      return dp.back();
    }
  };
}