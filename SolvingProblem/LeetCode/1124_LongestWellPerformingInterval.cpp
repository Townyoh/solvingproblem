#include <vector>
#include <unordered_map>
#include <algorithm>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/longest-well-performing-interval/description/
*/

// Difficult one for me. Help from the commu to resolve this one.
// Kind of exercice where I can't see the solution directly and have to work on paper :)

namespace LongestWellPerformingInterval
{
  class Solution
  {
  public:
    int longestWPI(vector<int>& hours) 
    {
      auto data = std::unordered_map<int, int>(hours.size());
      auto result = 0;
      auto count = 0;

      for (auto index = 0; index < hours.size(); index++)
      {
        count += (hours[index] > 8 ? 1 : -1);
        if (count > 0)
          result = index + 1;
        else
        {
          data.insert(std::make_pair(count, index));

          auto elem = data.find(count - 1);
          if (elem != data.end())
          {
            result = std::max(result, index - elem->second);
          }
        }
      }

      return result;
    }
  };
}