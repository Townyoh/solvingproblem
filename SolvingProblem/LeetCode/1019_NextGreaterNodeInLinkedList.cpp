#include <vector>
#include <stack>
#include <iso646.h>

using namespace std;

//Definition for singly-linked list.
struct ListNode 
{
  int val;
  ListNode *next;
  ListNode(int x)
    : val(x), next(nullptr)
  {}
 };
 
/*
  Link problem : https://leetcode.com/problems/next-greater-node-in-linked-list/description/
*/

namespace NextGreaterNodeInLinkedList
{
  class Solution
  {
    vector<int> largerNode;
    stack<int> nextNode;

  public:
    vector<int> nextLargerNodes(ListNode* head)
    {
      this->browseList(head, 0);

      return this->largerNode;
    }

  private:
    int insertValueInStack(int value)
    {
      while (not this->nextNode.empty())
      {
        if (value < this->nextNode.top())
          break;

        this->nextNode.pop();
      }

      auto greaterValue = this->nextNode.empty() ? 0 : this->nextNode.top();
      this->nextNode.push(value);

      return greaterValue;
    }

    void browseList(ListNode* head, int index)
    {
      if (head == nullptr)
        return;

      this->largerNode.push_back(0);

      if (head->next != nullptr)
        this->browseList(head->next, index + 1);

      auto greaterValue = this->insertValueInStack(head->val);
      this->largerNode[index] = greaterValue;
    }
  };
};
