#include <vector>
#include <algorithm>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/maximum-sum-of-two-non-overlapping-subarrays/description/
*/

/*
  Example here : 3, 8, 1, 3, 2, 1, 8, 9, 0
*/

namespace MaximumSumOfTwoNonOverlappingSubarrays
{
  class Solution
  {
  public:
    int maxSumTwoNoOverlap(vector<int>& data, int sizeFirst, int sizeSecond)
    {
      // Calcul prefix sum
      // Allow to calcul subarray (windows) with specific size easily 
      // With example we have : 3, 11, 12, 15, 17, 18, 26, 35, 35
      for (int index = 1; index < data.size(); index++)
        data[index] += data[index - 1];

      auto firstMax = data[sizeFirst - 1];
      auto secondMax = data[sizeSecond - 1];
      auto resMax = data[sizeFirst + sizeSecond - 1];

      for (int index = sizeFirst + sizeSecond; index < data.size(); index++)
      {
        // We just looking for the max subarray of specific size between the beginning (index 0) and (index - size of the other subarray)
        // Even if the 2 subarray max overlaps we will calcul the difference without overlaps
        firstMax = std::max(firstMax, data[index - sizeSecond] - data[index - sizeFirst - sizeSecond]);
        secondMax = std::max(secondMax, data[index - sizeFirst] - data[index - sizeFirst - sizeSecond]);

        // A better comprehension here is :
        // First line the first subarray is before the second subarray
        // Second line the second subarray is before the first subarray
        auto tempRes = std::max(
          data[index] + firstMax - data[index - sizeSecond],
          data[index] + secondMax - data[index - sizeFirst]
        );

        resMax = std::max(resMax, tempRes);
      }

      return resMax;
    }
  };
};

