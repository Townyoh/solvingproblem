#include <vector>
#include <queue>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/last-stone-weight/description/
*/

namespace LastStoneWeight
{
  class Solution 
  {
  public:
    int lastStoneWeight(vector<int>& stones) 
    {
      priority_queue<int> data;
      this->buildPriorityQueue(stones, data);

      while (data.size() > 1)
      {
        auto first = this->getAndDeleteTopElement(data);
        auto second = this->getAndDeleteTopElement(data);

        data.push(first - second);
      }

      return data.empty() ? 0 : data.top();
    }

  private:
    int getAndDeleteTopElement(priority_queue<int>& data)
    {
      auto topElem = data.top();
      data.pop();

      return topElem;
    }

    void buildPriorityQueue(const vector<int>& stones, priority_queue<int>& data)
    {
      for (const auto& stone : stones)
      {
        data.push(stone);
      }
    }
  };
}
