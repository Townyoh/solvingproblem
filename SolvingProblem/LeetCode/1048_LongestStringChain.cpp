#include <vector>
#include <map>
#include <algorithm>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/longest-string-chain/description/
*/

namespace LongestStringChain
{
  struct Word
  {
    string word;
    int value;

    Word(const string& w)
      : word(w), value(1)
    {}
  };

  class Solution 
  {
  public:
    int longestStrChain(vector<string>& words) 
    {
      map<int, vector<Word>> data;
      this->buildData(words, data);

      return this->findLongest(data);
    }

  private:
    int findLongest(map<int, vector<Word>>& data)
    {
      int maxValue = 1;

      for (auto& elem : data)
      {
        auto parentList = data.find(elem.first + 1);
        if (parentList != data.end())
        {
          for (auto& word : elem.second)
          {
            for (auto& parent : parentList->second)
            {
              if (parent.value <= word.value)
              {
                if (this->isPredecessor(word.word, parent.word))
                  parent.value = word.value + 1;

                maxValue = std::max(maxValue, parent.value);
              }
            }
          }
        }
      }

      return maxValue;
    }

    void buildData(const vector<string>& words, map<int, vector<Word>>& data)
    {
      for (const auto& word : words)
      {
        data[word.size()].push_back(Word(word));
      }
    }

    bool isPredecessor(const string& predecessor, const string& parent)
    {
      bool joker = false;
      auto iPred = 0;
      auto iParent = 0;
      
      while (iPred < predecessor.size())
      {
        if (predecessor[iPred] == parent[iParent])
          iPred++;
        else
        {
          if (joker)
            return false;

          joker = true;
        }
        iParent++;
      }

      if (iParent < parent.size() - 1)
        return false;

      return true;
    }
  };
}