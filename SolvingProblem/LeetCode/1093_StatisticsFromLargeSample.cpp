#include <vector>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/statistics-from-a-large-sample/description/
*/

namespace StatisticsFromLargeSample
{
  class Solution 
  {
  public:
    vector<double> sampleStats(vector<int>& count) 
    {
      auto result = vector<double>(5, -1);
      auto median = vector<pair<double, int>>();
      double counter = 0;
      double nbValue = 0;
      auto modeValue = 0;

      for (auto index = 0; index < count.size(); index++)
      {
        if (count[index] > 0)
        {
          median.push_back(make_pair(index, count[index]));
          nbValue += count[index];
          counter += (index * count[index]);
          result[1] = index;
          result[0] = result[0] > -1 ? result[0] : index;
          
          if (count[index] > modeValue)
          {
            modeValue = count[index];
            result[4] = index;
          }
        }
      }

      result[2] = (double)(counter / nbValue);
      result[3] = this->medianCalcul(nbValue, median);

      return result;
    }

  private:
    double medianCalcul(double nbValue, const vector<pair<double, int>>& median)
    {
      int half = nbValue / 2;
      auto counter = 0;
      auto index = 0;

      while (counter < half)
      {
        counter += median[index].second;
        index++;
      }

      double result = 0;

      if ((int)nbValue % 2 == 0)
      {
        if (counter == half)
          result = (double)((median[index].first + median[index - 1].first) / 2);
        else
          result = (double)((median[index - 1].first + median[index - 1].first) / 2);
      }
      else
        result = median[index - 1].first;

      return result;
    }
  };
}