#include <vector>
#include <unordered_map>
#include <algorithm>
#include <sstream>
#include <iso646.h>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/number-of-equivalent-domino-pairs/description/
*/

namespace NumberOfEquivalentDominoPairs
{
  class Solution 
  {
  public:
    int numEquivDominoPairs(vector<vector<int>>& dominoes) 
    {
      auto nbPair = 0;
      auto data = unordered_map<string, int>(dominoes.size());

      for (const auto& domino : dominoes)
      {
        auto low = std::min(domino[0], domino[1]);
        auto max = std::max(domino[0], domino[1]);

        auto os = stringstream();
        os << low << max;
        auto elem = data.insert(make_pair(os.str(), 1));

        if (not elem.second)
        {
          nbPair += elem.first->second;
          elem.first->second++;
        }
      }

      return nbPair;
    }
  };
}