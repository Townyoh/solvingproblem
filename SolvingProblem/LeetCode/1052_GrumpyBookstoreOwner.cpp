#include <vector>
#include <algorithm>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/grumpy-bookstore-owner/description/
*/

namespace GrumpyBookstoreOwner
{
  class Solution 
  {
  public:
    int maxSatisfied(vector<int>& customers, vector<int>& grumpy, int joker)
    {
      auto total = this->totalHappyCustomer(customers, grumpy);
      auto current = total;
      auto result = total;

      for (auto index = 0; index < customers.size(); index++)
      {
        if (grumpy[index] == 1)
          current += customers[index];
        if (index >= joker && grumpy[index - joker] == 1)
          current -= customers[index - joker];

        result = std::max(result, current);
      }

      return result;
    }

  private:
    int totalHappyCustomer(const vector<int>& customers, const vector<int>& grumpy)
    {
      auto total = 0;

      for (auto index = 0; index < customers.size(); index++)
      {
        total += (grumpy[index] == 0 ? customers[index] : 0);
      }

      return total;
    }
  };
}