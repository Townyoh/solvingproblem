#include <vector>
#include <queue>
#include <algorithm>
#include <iso646.h>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/height-checker/description/
*/

namespace HeightChecker
{
  class Solution 
  {
  public:
    int heightChecker(vector<int>& heights) 
    {
      auto diff = 0;
      auto heightsSorted = priority_queue<int, vector<int>, std::greater<int>>();
      for (const auto& elem : heights)
        heightsSorted.push(elem);

      auto i = 0;
      while (not heightsSorted.empty())
      {
        if (heightsSorted.top() != heights[i])
          diff++;

        i++;
        heightsSorted.pop();
      }

      return diff;
    }
  };
}