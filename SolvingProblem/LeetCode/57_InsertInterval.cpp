#include <vector>
#include <algorithm>
#include <iso646.h>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/insert-interval/description/
*/

namespace InsertInterval
{
  class Solution 
  {
  public:
    vector<vector<int>> insert(vector<vector<int>>& intervals, vector<int>& newInterval) 
    {
      auto result = vector<vector<int>>();

      for (const auto& interval : intervals)
      {
        if (interval[0] <= newInterval[0] && interval[1] >= newInterval[0])
        {
          result.push_back({ interval[0], std::max(interval[1], newInterval[1]) });
        }
        else if (not result.empty() && result.back()[1] >= interval[0])
          result.back()[1] = std::max(result.back()[1], interval[1]);
        else if ((newInterval[1] < interval[0] && result.empty()) ||
          (newInterval[1] < interval[0] && not result.empty() && result.back()[1] < newInterval[0]))
        {
          result.push_back(newInterval);
          result.push_back(interval);
        }
        else
        {
          if (newInterval[1] >= interval[0] && newInterval[1] <= interval[1])
            result.push_back({ std::min(interval[0], newInterval[0]), interval[1] });
          else if (newInterval[0] < interval[0] && newInterval[1] > interval[1])
            result.push_back(newInterval);
          else
            result.push_back(interval);
        }
      }

      if (result.empty() || newInterval[0] > result.back()[1])
        result.push_back(newInterval);

      return result;
    }
  };
}