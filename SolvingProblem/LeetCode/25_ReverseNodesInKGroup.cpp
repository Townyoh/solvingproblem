#include <iso646.h>
#include <algorithm>

//Definition for singly-linked list.
struct ListNode 
{
  int val;
  ListNode *next;
  ListNode(int x)
    : val(x), next(nullptr)
  {}
};

/*
  Link problem : https://leetcode.com/problems/reverse-nodes-in-k-group/description/
*/

namespace ReverseNodesInKGroup
{
  class Solution
  {
  public:
    ListNode* reverseKGroup(ListNode* head, int k)
    {
      auto current = head;
      ListNode* newHead = nullptr;
      ListNode* prev = nullptr;

      while (current != nullptr)
      {
        ListNode* newTail = current;
        auto headReverse = this->reverse(current, k);

        if (headReverse == nullptr)
        {
          if (prev != nullptr)
            prev->next = newTail;
          break;
        }

        if (newHead == nullptr)
          newHead = headReverse;

        if (prev != nullptr)
          prev->next = headReverse;
        prev = newTail;
      }

      if (newHead == nullptr)
        newHead = head;

      return newHead;
    }

  private:
    ListNode* reverse(ListNode*& head, int nb)
    {
      if (not this->enoughtNode(head, nb - 1))
        return nullptr;

      ListNode* prev = nullptr;
      ListNode* next = nullptr;
      for (auto count = 0; count < nb; count++)
      {
        next = head->next;
        head->next = prev;
        prev = head;

        head = next;
      }

      return prev;
    }

    bool enoughtNode(ListNode* head, int nb)
    {
      while (head->next != nullptr && nb > 0)
      {
        head = head->next;
        nb -= 1;
      }

      return nb == 0;
    }
  };
}