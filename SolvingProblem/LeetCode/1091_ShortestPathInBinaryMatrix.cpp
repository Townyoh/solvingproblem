#include <vector>
#include <queue>
#include <iso646.h>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/shortest-path-in-binary-matrix/description/
*/

namespace ShortestPathInBinaryMatrix
{
  class Solution 
  {
    int finalRow;
    int finalCol;
    int result;
    vector<int> dRow;
    vector<int> dCol;

  public:
    int shortestPathBinaryMatrix(vector<vector<int>>& grid) 
    {
      auto data = vector<vector<int>>(grid.size(), vector<int>(grid[0].size(), 0));
      this->finalCol = grid[grid.size() - 1].size() - 1;
      this->finalRow = grid.size() - 1;
      this->result = -1;

      if (grid[0][0] == 1 || grid[this->finalRow][this->finalCol] == 1)
        return this->result;

      this->dRow = { -1, -1, 0, 1, 1, 1, 0, -1 };
      this->dCol = { 0, 1, 1, 1, 0, -1, -1, -1 };
      data[0][0] = 1;
      data[this->finalRow][this->finalCol] = 1;
      grid[0][0] = 2;
      grid[this->finalRow][this->finalCol] = 3;

      this->doubleBFS(data, grid);

      return this->result;
    }

  private:
    bool validationNewPoint(vector<vector<int>>& grid, int row, int col)
    {
      if (row < 0 || row >= grid.size() || col < 0 || col >= grid[row].size() || grid[row][col] == 1)
        return false;

      

      return true;
    }

    bool browseQueue(vector<vector<int>>& data, vector<vector<int>>& grid, queue<pair<int, int>>& fifo, int color)
    {
      for (auto size = fifo.size(); size > 0; size--)
      {
        auto elem = fifo.front();
        fifo.pop();

        for (auto direction = 0; direction < this->dRow.size(); direction++)
        {
          auto nextRow = elem.first + this->dRow[direction];
          auto nextCol = elem.second + this->dCol[direction];

          if (this->validationNewPoint(grid, nextRow, nextCol))
          {
            if (data[nextRow][nextCol] == 0)
            {
              fifo.push(make_pair(nextRow, nextCol));
              data[nextRow][nextCol] = data[elem.first][elem.second] + 1;
            }
            else if (grid[nextRow][nextCol] != 0 && grid[nextRow][nextCol] != color)
            {
              this->result = data[nextRow][nextCol] + data[elem.first][elem.second];
              return true;
            }

            grid[nextRow][nextCol] = color;
          }
        }
      }

      return false;
    }

    void doubleBFS(vector<vector<int>>& data, vector<vector<int>>& grid)
    {
      auto beginQueue = queue<pair<int, int>>();
      beginQueue.push(make_pair(0, 0));
      auto endQueue = queue<pair<int, int>>();
      endQueue.push(make_pair(this->finalRow, this->finalCol));

      while (not beginQueue.empty() && not endQueue.empty())
      {
        if (this->browseQueue(data, grid, beginQueue, 2))
          break;
        if (this->browseQueue(data, grid, endQueue, 3))
          break;
      }
    }
  };
}