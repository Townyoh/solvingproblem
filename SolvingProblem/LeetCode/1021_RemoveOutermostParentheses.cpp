#include <string>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/remove-outermost-parentheses/description/
*/

namespace RemoveOutermostParentheses
{
  class Solution
  {
  public:
    string removeOuterParentheses(string str)
    {
      string clean;
      auto openParentheses = 0;

      for (int index = 0; index < str.size(); index++)
      {
        if (str[index] == '(')
        {
          if (openParentheses > 0)
            clean.push_back(str[index]);
          openParentheses++;
        }
        else
        {
          if (openParentheses > 1)
            clean.push_back(str[index]);
          openParentheses--;
        }
      }

      return clean;
    }
  };
};

