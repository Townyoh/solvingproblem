#include <vector>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/partition-array-into-three-parts-with-equal-sum/description/
*/

namespace PartitionArrayIntoThreePartsWithEqualSum
{
  class Solution 
  {
  public:
    bool canThreePartsEqualSum(vector<int>& list) 
    {
      auto sum = this->sumArray(list);
      
      if (sum % 3 != 0)
        return false;
      
      auto tier = sum / 3;
      auto index = 0;
      index = this->findTierFromIndex(list, index, tier);
      if (index == -1)
        return false;
      index = this->findTierFromIndex(list, index, tier);
      if (index == -1)
        return false;
      index = this->findTierFromIndex(list, index, tier);
      if (index == -1)
        return false;

      return true;
    }

  private:
    int findTierFromIndex(const vector<int>& list, int index, int tier)
    {
      auto current = 0;

      for (; index < list.size(); index++)
      {
        if (current == tier)
          return index;
        
        current += list[index];
      }

      return (current != tier ? -1 : index);
    }

    int sumArray(const vector<int>& list)
    {
      auto sum = 0;

      for (const auto& elem : list)
        sum += elem;

      return sum;
    }
  };
}