#include <vector>
#include <unordered_map>
#include <iso646.h>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/best-sightseeing-pair/description/
*/

namespace SubstringWithConcatenationOfAllWords
{
  class Solution 
  {
  public:
    vector<int> findSubstring(string str, vector<string>& words) 
    {
      auto result = vector<int>();

      if (words.empty())
        return result;

      auto data = unordered_map<string, int>(words.size());
      this->buildData(words, data);
      auto wordSize = words[0].size();
      auto maxSize = str.size() - (wordSize * words.size()) + 1;

      for (auto index = 0; index <= maxSize; )
      {
        if (this->find(index, data, str, wordSize, maxSize))
          result.push_back(index);

        index++;
      }

      return result;
    }

  private:
    bool find(int index, unordered_map<string, int> data, const string& str, int wordSize, int maxSize)
    {
      while (index <= maxSize)
      {
        auto sub = str.substr(index, wordSize);

        auto elem = data.find(sub);
        if (elem == data.end())
          break;

        elem->second--;
        if (elem->second <= 0)
          data.erase(elem->first);

        if (data.empty())
          break;

        index += wordSize;
      }

      return data.empty();
    }

    void buildData(const vector<string>& words, unordered_map<string, int>& data)
    {
      for (const auto& word : words)
      {
        auto elem = data.insert(make_pair(word, 1));

        if (not elem.second)
          elem.first->second++;
      }
    }
  };
}