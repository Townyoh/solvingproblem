#include <vector>
#include <algorithm>
#include <map>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/car-pooling/description/
*/

namespace CarPooling
{
  class Solution 
  {
  public:
    bool carPooling(vector<vector<int>>& trips, int capacity) 
    {
      auto data = map<int, int>();
      for (const auto& trip : trips)
      {
        data[trip[1]] += trip[0];
        data[trip[2]] -= trip[0];
      }

      auto currentCapacity = 0;
      for (const auto& elem : data)
      {
        currentCapacity += elem.second;

        if (currentCapacity > capacity)
          return false;
      }

      return true;
    }
  };
}