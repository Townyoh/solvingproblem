#include <vector>
#include <queue>
#include <iso646.h>

using namespace std;

/*
Description :

On an 8x8 chessboard, there can be multiple Black Queens and one White King.

Given an array of integer coordinates queens that represents the positions of the Black Queens, 
and a pair of coordinates king that represent the position of the White King, 
return the coordinates of all the queens (in any order) that can attack the King.
*/

/*
  Link problem : https://leetcode.com/problems/queens-that-can-attack-the-king/description/
*/

namespace QueensThatCanAttackTheKing
{
  class Solution 
  {

  public:
    vector<vector<int>> queensAttacktheKing(vector<vector<int>>& queens, vector<int>& king) 
    {
      auto grid = vector<vector<bool>>(8, vector<bool>(8, false));
      this->buildGrid(queens, grid);
      auto result = vector<vector<int>>();

      this->browseAllDirections(grid, king[0], king[1], result);

      return result;
    }

  private:
    void browseAllDirections(vector<vector<bool>>& grid, int kingRow, int kingCol, vector<vector<int>>& result)
    {
      for (auto dRow = -1; dRow <= 1; dRow++)
      {
        for (auto dCol = -1; dCol <= 1; dCol++)
        {
          if (dRow == 0 && dCol == 0)
            continue;

          auto row = kingRow + dRow;
          auto col = kingCol + dCol;

          while (row >= 0 && row < 8 && col >= 0 && col < 8)
          {
            if (grid[row][col])
            {
              result.push_back({ row, col });
              break;
            }
            row += dRow;
            col += dCol;
          }
        }
      }
    }

    void buildGrid(const vector<vector<int>>& queens, vector<vector<bool>>& grid)
    {
      for (const auto& elem : queens)
      {
        grid[elem[0]][elem[1]] = true;
      }
    }
  };
}