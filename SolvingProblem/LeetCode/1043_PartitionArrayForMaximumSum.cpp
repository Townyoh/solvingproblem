#include <vector>
#include <algorithm>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/partition-array-for-maximum-sum/description/
*/
namespace PartitionArrayForMaximumSum
{
  class Solution 
  {
  public:
    int maxSumAfterPartitioning(vector<int>& list, int size) 
    {
      auto data = vector<int>(list.size() + 1, 0);

      for (int index = 0; index < list.size(); index++)
      {
        auto maxSoFar = list[index];
        data[index + 1] = data[index] + maxSoFar;
        for (auto count = 1; count < size; count++)
        {
          if (index - count < 0)
            break;

          maxSoFar = std::max(maxSoFar, list[index - count]); 
          data[index + 1] = std::max(data[index + 1], data[index - count] + maxSoFar * (count + 1));
        }
      }

      return data.back();
    }
  };
}