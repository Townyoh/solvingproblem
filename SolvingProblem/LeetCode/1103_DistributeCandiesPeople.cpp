#include <vector>
#include <algorithm>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/distribute-candies-to-people/description/
*/

namespace DistributeCandiesPeople
{
  class Solution 
  {
  public:
    vector<int> distributeCandies(int candies, int people)
    {
      auto result = vector<int>(people, 0);
      auto counter = 0;

      while (candies > 0)
      {
        result[counter % people] += std::min(counter + 1, candies);
        candies -= counter + 1;
        counter++;
      }

      return result;
    }
  };
}