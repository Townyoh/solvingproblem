#include <string>
#include <iso646.h>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/remove-all-adjacent-duplicates-in-string/description/
*/


namespace RemoveAllAdjacentDuplicatesInString
{
  class Solution 
  {
  public:
    string removeDuplicates(string str) 
    {
      auto result = string{};

      for (const auto& letter : str)
      {
        if (not result.empty() && result.back() == letter)
          result.pop_back();
        else
          result.push_back(letter);
      }

      return result;
    }
  };
}