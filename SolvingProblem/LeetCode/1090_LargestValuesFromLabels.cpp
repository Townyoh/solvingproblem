#include <vector>
#include <unordered_map>
#include <queue>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/largest-values-from-labels/description/
*/

namespace LargestValuesFromLabels
{

  struct Compare
  {
    bool operator()(const pair<int, int>& one, const pair<int, int>& two)
    {
      if (one.first == two.first)
        return one.second > two.second;

      return one.first < two.first;
    }
  };

  class Solution 
  {
  public:
    int largestValsFromLabels(vector<int>& values, vector<int>& labels, int num_wanted, int use_limit)
    {
      priority_queue<pair<int, int>, vector<pair<int, int>>, Compare> priority;
      unordered_map<int, int> data;
      this->buildPriority(priority, values, labels);
      auto result = 0;

      for (auto count = 0; count < num_wanted; )
      {
        result += this->addTopElement(priority, data, count, use_limit);

        if (priority.empty())
          break;
      }

      return result;
    }

  private:
    int addTopElement(priority_queue<pair<int, int>, vector<pair<int, int>>, Compare>& priority,
      unordered_map<int, int>& data, int& count, int use_limit)
    {
      auto topElem = priority.top();
      priority.pop();
      auto valueToAdd = topElem.first;

      auto elem = data.find(topElem.second);
      if (elem != data.end() && elem->second >= use_limit)
        valueToAdd = 0;
      else
        count++;

      data[topElem.second]++;
     
      return valueToAdd;
    }

    void buildPriority(priority_queue<pair<int, int>, vector<pair<int, int>>, Compare>& priority,
      const vector<int>& values, const vector<int>& labels)
    {
      for (auto index = 0; index < values.size(); index++)
      {
        priority.push(make_pair(values[index], labels[index]));
      }
    }
  };
}