
  //Definition for a binary tree node.
struct TreeNode 
{
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode(int x)
    : val(x), left(nullptr), right(nullptr)
  {}
};
 
/*
  Link problem : https://leetcode.com/problems/sum-of-root-to-leaf-binary-numbers/description/
*/

namespace SumOfRootToLeafBinaryNumbers
{
  class Solution
  {
    int sum;
    int current;

  public:
    int sumRootToLeaf(TreeNode* root)
    {
      this->sum = 0;
      this->current = 0;

      this->preOrder(root);

      return this->sum;
    }

  private:
    void preOrder(TreeNode* node)
    {
      if (node == nullptr)
        return;

      this->current = this->current << 1;
      this->current += node->val;

      if (node->left == nullptr && node->right == nullptr)
        this->sum += this->current;

      this->preOrder(node->left);
      this->preOrder(node->right);

      this->current = this->current >> 1;
    }
  };
};

