#include <vector>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/binary-prefix-divisible-by-5/description/
*/

namespace BinaryPrefixDivisibleByFive
{
  class Solution
  {
  public:
    vector<bool> prefixesDivBy5(vector<int>& list)
    {
      auto result = vector<bool>(list.size(), false);
      unsigned long long int value = 0;

      for (auto index = 0; index < list.size(); index++)
      {
        value = value << 1;
        value += list[index];

        if (value % 5 == 0)
        {
          result[index] = true;
          value = 0;
        }
      }

      return result;
    }
  };
};
