#include <vector>
#include <unordered_set>
#include <iso646.h>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/delete-nodes-and-return-forest/description/
*/

// Definition given by exercice
struct TreeNode
{
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode(int x)
    : val(x), left(nullptr), right(nullptr)
  {}
};

namespace DeleteNodesAndReturnForest
{
 class Solution 
  {
  public:
    vector<TreeNode*> delNodes(TreeNode* root, vector<int>& toDelete) 
    {
      auto dict = std::unordered_set<int>(toDelete.begin(), toDelete.end());
      auto result = std::vector<TreeNode*>();

      this->preOrderTraversal(root, result, dict, true);

      return result;
    }

  private:
    bool preOrderTraversal(TreeNode* node, std::vector<TreeNode*>& result, const std::unordered_set<int>& dict, bool shouldAdd) const
    {
      if (node == nullptr)
        return true;

      bool childShouldBeAdded = (dict.find(node->val) == dict.end() ? false : true);

      if (this->preOrderTraversal(node->left, result, dict, childShouldBeAdded))
        node->left = nullptr;
      if (this->preOrderTraversal(node->right, result, dict, childShouldBeAdded))
        node->right = nullptr;

      if (shouldAdd && not childShouldBeAdded)
        result.push_back(node);

      return childShouldBeAdded;
    }
  };
}