#include <string>
#include <iso646.h>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/greatest-common-divisor-of-strings/description/
*/

namespace GreatestCommonDivisorOfStrings
{
  class Solution 
  {
  public:
    string gcdOfStrings(string str1, string str2) 
    {
      auto maxSize = (str1.size() > str2.size()) ? str2.size() : str1.size();

      while (maxSize > 0)
      {
        if (str1.size() % maxSize == 0 && str2.size() % maxSize == 0)
        {
          if (this->compareSubstring(str1, str1.substr(0, maxSize)) &&
            this->compareSubstring(str2, str1.substr(0, maxSize)))
            break;
        }

        maxSize--;
      }

      return str1.substr(0, maxSize);
    }

  private:
    bool compareSubstring(const string& ref, const string& sub)
    {
      auto subSize = sub.size();
      for (auto index = 0; index < ref.size(); index += subSize)
      {
        if (sub != ref.substr(index, subSize))
          return false;
      }

      return true;
    }
  };
}