#include <vector>
#include <algorithm>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/two-city-scheduling/description/
*/

/*
  Again a problem with tricky solution.
  Got this solution with community's help
*/
namespace TwoCityScheduling
{
  class Solution
  {
  public:
    int twoCitySchedCost(vector<vector<int>>& costs)
    {
      std::sort(costs.begin(), costs.end(),
        [](const vector<int>& first, const vector<int>& second)
      {
        return ((first[0] - first[1]) < (second[0] - second[1]));
      });

      auto count = 0;
      for (auto index = 0; index < costs.size() / 2; index++)
      {
        count += (costs[index][0] + costs[index + costs.size() / 2][1]);
      }

      return count;
    }
  };
};

