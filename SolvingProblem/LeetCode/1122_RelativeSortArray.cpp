#include <vector>
#include <unordered_map>
#include <iso646.h>
#include <algorithm>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/relative-sort-array/description/
*/

namespace RelativeSortArray
{
  class Solution 
  {
  public:
    vector<int> relativeSortArray(vector<int>& arr1, vector<int>& arr2)
    {
      auto data = unordered_map<int, int>(arr1.size());
      this->buildMap(arr1, data);

      auto result = vector<int>();
      this->firstStep(result, data, arr2);
      this->secondStep(result, data);

      return result;
    }

  private:
    void secondStep(vector<int>& result, unordered_map<int, int>& data)
    {
      auto temp = vector<int>();
      for (const auto& elem : data)
      {
        temp.insert(temp.end(), elem.second, elem.first);
      }

      std::sort(temp.begin(), temp.end());

      result.insert(result.end(), temp.begin(), temp.end());
    }

    void firstStep(vector<int>& result, unordered_map<int, int>& data, const vector<int>& arr2)
    {
      for (const auto& elem : arr2)
      {
        auto findElem = data.find(elem);
        if (findElem != data.end())
        {
          result.insert(result.end(), findElem->second, findElem->first);
          data.erase(findElem);
        }
      }
    }

    void buildMap(const vector<int>& arr1, unordered_map<int, int>& data)
    {
      for (const auto& elem : arr1)
      {
        auto elemInserted = data.insert(make_pair(elem, 1));

        if (not elemInserted.second)
          elemInserted.first->second++;
      }
    }
  };
}