#include <vector>
#include <string>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/stream-of-characters/description/
*/

namespace StreamOfCharacters
{
  class StreamChecker
  {
    class Trie
    {
      struct Node
      {
        bool isEnd;
        vector<unique_ptr<Node>> childs;

        Node()
          : isEnd(false)
        {
          childs = vector<unique_ptr<Node>>(26);
        }
      };

      unique_ptr<Node> root;

    public:
      Trie()
      {
        root = make_unique<Node>();
      }

      void addWord(const string& word)
      {
        this->addLetter(root.get(), word, word.size() - 1);
      }

      bool isWordExist(const string& word)
      {
        return this->isExist(root.get(), word, word.size() - 1);
      }

    private:
      bool isExist(Node* node, const string& word, int index)
      {
        if (index >= word.size())
          return false;

        int letter = word[index] - 'a';

        if (node->childs[letter] == nullptr)
          return false;

        if (node->childs[letter]->isEnd)
          return true;

        return this->isExist(node->childs[letter].get(), word, index - 1);
      }

      void addLetter(Node* node, const string& word, int index)
      {
        if (index < 0)
          return;

        int letter = word[index] - 'a';

        if (node->childs[letter] == nullptr)
        {
          auto child = make_unique<Node>();
          node->childs[letter] = std::move(child);
        }

        if (index == 0)
          node->childs[letter]->isEnd = true;

        addLetter(node->childs[letter].get(), word, index - 1);
      }
    };


  public:
    Trie reverseTrie;
    string stringQuerie;

    StreamChecker(vector<string>& words)
    {
      reverseTrie = Trie();
      stringQuerie = string{};

      for (const auto& word : words)
        reverseTrie.addWord(word);
    }

    bool query(char letter)
    {
      this->stringQuerie.push_back(letter);

      return this->reverseTrie.isWordExist(this->stringQuerie);
    }
  };
};

