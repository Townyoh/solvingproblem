#include <string>
#include <unordered_set>
#include <algorithm>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/letter-tile-possibilities/description/
*/

namespace LetterTilePossibilities
{
  class Solution 
  {
  public:
    int numTilePossibilities(string tiles) 
    {
      unordered_set<string> data;

      this->helper(tiles, 0, data);

      return data.size();
    }

  private:
    void helper(string& tiles, int index, unordered_set<string>& data)
    {
      for (auto current = index; current < tiles.size(); current++)
      {
        std::swap(tiles[index], tiles[current]);
        
        auto elem = data.insert(tiles.substr(0, index + 1));
        if (elem.second)
          this->helper(tiles, index + 1, data);

        std::swap(tiles[index], tiles[current]);
      }
    }
  };
}