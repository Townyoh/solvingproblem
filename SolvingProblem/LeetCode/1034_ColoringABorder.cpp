#include <vector>
#include <queue>
#include <iso646.h>
#include <utility>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/coloring-a-border/description/
*/

namespace ColoringABorder
{
  class Solution
  {
    int refColor;


  public:
    vector<vector<int>> colorBorder(vector<vector<int>>& grid, int r0, int c0, int color)
    {
      this->refColor = grid[r0][c0];
      vector<vector<bool>> alreadyVisited(grid.size(), vector<bool>(grid[0].size(), false));
      queue<pair<int, int>> fifo;
      this->checkAllConnectedCell(grid, r0, c0, color, alreadyVisited, fifo);
      alreadyVisited[r0][c0] = true;

      while (not fifo.empty())
      {
        for (int count = fifo.size(); count > 0; count--)
        {
          auto address = fifo.front();
          fifo.pop();
          this->checkAllConnectedCell(grid, address.first, address.second, color, alreadyVisited, fifo);
        }
      }

      return grid;
    }

  private:
    bool isNeighborIsSameAsRef(const vector<vector<int>>& grid, int row, int col)
    {
      if (row < 0 || col < 0 || row >= grid.size() || col >= grid[row].size() || grid[row][col] != this->refColor)
        return false;

      return true;
    }

    void checkCell(vector<vector<int>>& grid, int originRow, int originCol, int neighborRow, int neighborCol, int color, vector<vector<bool>>& alreadyVisited, queue<pair<int, int>>& fifo)
    {
      if (neighborRow < 0 || neighborCol < 0 || neighborRow >= grid.size() || neighborCol >= grid[neighborRow].size())
      {
        grid[originRow][originCol] = color;
        return;
      }

      if (grid[neighborRow][neighborCol] == this->refColor && not alreadyVisited[neighborRow][neighborCol])
      {
        alreadyVisited[neighborRow][neighborCol] = true;
        fifo.push(make_pair(neighborRow, neighborCol));
        return;
      }

      if (grid[neighborRow][neighborCol] != this->refColor && not alreadyVisited[neighborRow][neighborCol])
      {
        grid[originRow][originCol] = color;
        return;
      }
    }

    void checkAllConnectedCell(vector<vector<int>>& grid, int row, int col, int color, vector<vector<bool>>& alreadyVisited, queue<pair<int, int>>& fifo)
    {
      this->checkCell(grid, row, col, row - 1, col, color, alreadyVisited, fifo);
      this->checkCell(grid, row, col, row, col + 1, color, alreadyVisited, fifo);
      this->checkCell(grid, row, col, row + 1, col, color, alreadyVisited, fifo);
      this->checkCell(grid, row, col, row, col - 1, color, alreadyVisited, fifo);
    }
  };
};

