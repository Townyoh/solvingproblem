#include <unordered_map>

using namespace std;

//Definition for singly-linked list provided by exercice
struct ListNode 
{
    int val;
    ListNode *next;
    ListNode(int x)
      : val(x), next(nullptr)
    {}
};

/*
  Link problem : https://leetcode.com/problems/remove-zero-sum-consecutive-nodes-from-linked-list/description/
*/

/*
Description:
Given the head of a linked list, we repeatedly delete consecutive sequences of nodes that sum to 0 until there are no such sequences.

After doing so, return the head of the final linked list.  
You may return any such answer.
*/

namespace RemoveZeroSumConsecutiveNodesFromLinkedList
{
  class Solution 
  {
  public:
    ListNode* removeZeroSumSublists(ListNode* head) 
    {
      auto data = unordered_map<int, ListNode*>();
      data.insert(make_pair(0, nullptr));
      auto current = head;
      auto currentValue = 0;

      while (current != nullptr)
      {
        currentValue += current->val;

        auto elem = data.find(currentValue);
        if (elem != data.end())
        {
          auto back = current;

          while (back != elem->second)
          {
            currentValue -= back->val;
            back = data.find(currentValue)->second;
            if (back == elem->second)
              break;
            data.erase(currentValue);
          }

          if (back == nullptr)
            head = current->next;
          else
            back->next = current->next;
        }
        else
        {
          data.insert(make_pair(currentValue, current));
        }

        current = current->next;
      }

      return head;
    }
  };
}