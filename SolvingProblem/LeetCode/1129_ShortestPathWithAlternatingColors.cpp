#include <vector>
#include <queue>
#include <unordered_map>
#include <iso646.h>

using namespace std;

/*
  Link problem : https://leetcode.com/problems/shortest-path-with-alternating-colors/description/
*/

namespace ShortestPathWithAlternatingColors
{
  class Solution 
  {
    struct Node
    {
      int value;
      bool red;
      bool blue;

      Node(int v, bool r, bool b)
        : value(v), red(r), blue(b)
      {}

      Node()
      {
        value = -1;
        red = false;
        blue = false;
      }
    };

    Node root;

  public:
    vector<int> shortestAlternatingPaths(int nbNode, vector<vector<int>>& red, vector<vector<int>>& blue) 
    {
      root = Node(0, true, true);
      auto graph = vector<vector<Node>>(nbNode, vector<Node>());
      this->buildGraph(graph, red, true);
      this->buildGraph(graph, blue, false);

      auto result = vector<int>(nbNode, 0);

      for (auto index = 1; index < nbNode; index++)
      {
        auto lenght = this->bfs(graph, index);
        result[index] = lenght;
      }

      return result;
    }

  private:
    int bfs(const vector<vector<Node>>& graph, int nodeToFind)
    {
      auto fifo = queue<Node>();
      auto alreadySeen = unordered_map<int, int>(graph.size());
      fifo.push(root);
      auto count = 1;

      while (not fifo.empty())
      {
        for (auto size = fifo.size(); size > 0; size--)
        {
          auto current = fifo.front();
          fifo.pop();

          if (this->addEdgeNeighbor(graph, fifo, alreadySeen, current, nodeToFind))
            return count;
        }

        count++;
      }

      return -1;
    }

    int valueOfParent(const Node& parent)
    {
      auto value = 0;
      value += parent.red ? 1 : 0;
      value += parent.blue ? 2 : 0;

      return value;
    }

    bool addEdgeNeighbor(const vector<vector<Node>>& graph, queue<Node>& fifo, unordered_map<int, int>& alreadySeen, Node parent, int nodeToFind)
    {
      for (const auto& neigh : graph[parent.value])
      {
        auto colorEdge = this->valueOfParent(neigh);
        auto elem = alreadySeen.find(neigh.value);

        if (elem == alreadySeen.end())
        {
          if (parent.value == 0 || parent.blue && parent.red || parent.blue && neigh.red || parent.red && neigh.blue)
          {
            fifo.push(neigh);
            alreadySeen.insert(make_pair(neigh.value, colorEdge));
            if (neigh.value == nodeToFind)
              return true;
          }
        }
        else
        {
          if (elem->second == 3)
            continue;
          else if (elem->second == 1 && parent.red && neigh.blue)
          {
            fifo.push(neigh);
            elem->second = 3;
            if (neigh.value == nodeToFind)
              return true;
          }
          else if (elem->second == 2 && parent.blue && neigh.red)
          {
            fifo.push(neigh);
            elem->second = 3;
            if (neigh.value == nodeToFind)
              return true;
          }
        }
      }

      return false;
    }

    void buildGraph(vector<vector<Node>>& graph, const vector<vector<int>>& edges, bool isRed)
    {
      for (const auto& edge : edges)
      {
        Node node(edge[1], isRed, !isRed);
        graph[edge[0]].push_back(node);
      }
    }
  };
}