// Definition for a binary tree node.
struct TreeNode
{
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode(int x) :
    val(x), left(nullptr), right(nullptr) {}
};

/*
Description:

Given a binary tree root and an integer target, delete all the leaf nodes with value target.

Note that once you delete a leaf node with value target, if it's parent node becomes a leaf node and has the value target.
It should also be deleted (you need to continue doing that until you can't).
*/

/*
  Link problem : https://leetcode.com/problems/delete-leaves-with-a-given-value/description/
*/

namespace DeleteLeavesWithAGivenValue
{
  class Solution
  {
  public:
    TreeNode* removeLeafNodes(TreeNode* root, int target)
    {
      return remove(root, target);
    }

  private:
    TreeNode* remove(TreeNode* node, int target)
    {
      if (node->left != nullptr)
        node->left = this->remove(node->left, target);

      if (node->right != nullptr)
        node->right = this->remove(node->right, target);

      if (node->val == target && node->left == nullptr && node->right == nullptr)
        return nullptr;

      return node;
    }
  };
}
