#include <string>

using namespace std;

/*
Description :

Balanced strings are those who have equal quantity of 'L' and 'R' characters.

Given a balanced string s split it in the maximum amount of balanced strings.

Return the maximum amount of splitted balanced strings.
*/

/*
  Link problem : https://leetcode.com/problems/split-a-string-in-balanced-strings/description/
*/


namespace SplitAStringInBalancedStrings
{
  class Solution 
  {
  public:
    int balancedStringSplit(string str) 
    {
      auto result = 0;
      auto count = 0;

      for (auto index = 0; index < str.size(); index++)
      {
        if (str[index] == 'L')
          count++;
        else
          count--;

        if (count == 0)
          result++;
      }

      return result;
    }
  };
}