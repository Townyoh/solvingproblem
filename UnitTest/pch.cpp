//
// pch.cpp
// Include the standard header and generate the precompiled header.
//

#include "pch.h"

struct TreeNode
{
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode(int x)
    : val(x), left(nullptr), right(nullptr)
  {}
};

struct ListNode
{
  int val;
  ListNode *next;
  ListNode(int x)
    : val(x), next(nullptr)
  {}
};

::testing::AssertionResult IS_EQUAL_TREE(TreeNode* ref, TreeNode* compare)
{
  if (ref == nullptr && compare == nullptr)
    return ::testing::AssertionSuccess();

  if ((ref == nullptr && compare != nullptr) || (ref != nullptr && compare == nullptr))
    return ::testing::AssertionFailure();

  if (ref->val != compare->val)
    return ::testing::AssertionFailure();

  if (IS_EQUAL_TREE(ref->left, compare->left) == ::testing::AssertionFailure())
    return ::testing::AssertionFailure();

  if (IS_EQUAL_TREE(ref->right, compare->right) == ::testing::AssertionFailure())
    return ::testing::AssertionFailure();

  return ::testing::AssertionSuccess();
}

::testing::AssertionResult IS_EQUAL_VECTOR_TREE(const vector<TreeNode*>& first, const vector<TreeNode*>& second)
{
  if (first.size() != second.size())
    return ::testing::AssertionFailure();

  for (auto index = 0; index < first.size(); index++)
  {
     if (IS_EQUAL_TREE(first[index], second[index]) == ::testing::AssertionFailure())
       return ::testing::AssertionFailure();
  }

  return ::testing::AssertionSuccess();
}

::testing::AssertionResult IS_EQUAL_LINKED_LIST(ListNode* ref, ListNode* compare)
{
  while (ref != nullptr && compare != nullptr)
  {
    if (ref->val != compare->val)
      return ::testing::AssertionFailure();

    ref = ref->next;
    compare = compare->next;
  }

  return ref == compare ? ::testing::AssertionSuccess() : ::testing::AssertionFailure();
}
