#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1071_GreatestCommonDivisorOfStrings.cpp"

TEST(GreatestCommonDivisorOfStrings, First)
{
  auto str1 = string("ABCABC");
  auto str2 = string("ABC");
  auto result = string("ABC");

  GreatestCommonDivisorOfStrings::Solution solver;

  EXPECT_EQ(solver.gcdOfStrings(str1, str2), result);
}

TEST(GreatestCommonDivisorOfStrings, Second)
{
  auto str1 = string("ABABAB");
  auto str2 = string("ABAB");
  auto result = string("AB");

  GreatestCommonDivisorOfStrings::Solution solver;

  EXPECT_EQ(solver.gcdOfStrings(str1, str2), result);
}

TEST(GreatestCommonDivisorOfStrings, Third)
{
  auto str1 = string("LEET");
  auto str2 = string("CODE");
  auto result = string("");

  GreatestCommonDivisorOfStrings::Solution solver;

  EXPECT_EQ(solver.gcdOfStrings(str1, str2), result);
}
