#include "../pch.h"
#include "../../SolvingProblem/LeetCode/51_NQueens.cpp"

TEST(NQueens, First)
{
  auto input = 4;

  auto expected = vector<vector<string>>
  {
    {
      ".Q..",
      "...Q",
      "Q...",
      "..Q."
    },
    {
      "..Q.",
      "Q...",
      "...Q",
      ".Q.."
    }
  };

  NQueens::Solution solver;

  EXPECT_TRUE(IS_EQUAL_VECTOR(solver.solveNQueens(input), expected));
}