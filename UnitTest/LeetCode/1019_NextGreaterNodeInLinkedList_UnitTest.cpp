#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1019_NextGreaterNodeInLinkedList.cpp"

TEST(NextGreaterNodeInLinkedList, First)
{
  NextGreaterNodeInLinkedList::Solution solver;

  ListNode *head = new ListNode(2);
  head->next = new ListNode(1);
  head->next->next = new ListNode(5);

  vector<int> result = { 5, 5, 0 };

  EXPECT_EQ(solver.nextLargerNodes(head), result);
}

TEST(NextGreaterNodeInLinkedList, Second)
{
  NextGreaterNodeInLinkedList::Solution solver;

  ListNode *head = new ListNode(2);
  head->next = new ListNode(7);
  head->next->next = new ListNode(4);
  head->next->next->next = new ListNode(3);
  head->next->next->next->next = new ListNode(5);

  vector<int> result{ 7, 0, 5, 5, 0 };

  EXPECT_EQ(solver.nextLargerNodes(head), result);
}

TEST(NextGreaterNodeInLinkedList, Third)
{
  NextGreaterNodeInLinkedList::Solution solver;

  ListNode *head = new ListNode(1);
  head->next = new ListNode(7);
  head->next->next = new ListNode(5);
  head->next->next->next = new ListNode(1);
  head->next->next->next->next = new ListNode(9);
  head->next->next->next->next->next = new ListNode(2);
  head->next->next->next->next->next->next = new ListNode(5);
  head->next->next->next->next->next->next->next = new ListNode(1);

  vector<int> result{ 7, 9, 9, 9, 0, 5, 0, 0 };

  EXPECT_EQ(solver.nextLargerNodes(head), result);
}


