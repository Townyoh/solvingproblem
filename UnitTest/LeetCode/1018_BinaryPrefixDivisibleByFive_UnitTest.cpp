#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1018_BinaryPrefixDivisibleByFive.cpp"

TEST(BinaryPrefixDivisibleByFive, First)
{
  BinaryPrefixDivisibleByFive::Solution solver;

  vector<int> data = { 1, 1, 1 };
  vector<bool> result = { false, false, false };

  EXPECT_EQ(solver.prefixesDivBy5(data), result);
}

TEST(BinaryPrefixDivisibleByFive, Second)
{
  BinaryPrefixDivisibleByFive::Solution solver;

  vector<int> data = { 0, 1, 1 };
  vector<bool> result = { true, false, false };

  EXPECT_EQ(solver.prefixesDivBy5(data), result);
}

TEST(BinaryPrefixDivisibleByFive, Third)
{
  BinaryPrefixDivisibleByFive::Solution solver;

  vector<int> data = { 0, 1, 1, 1, 1, 1 };
  vector<bool> result = { true, false, false, false, true, false };

  EXPECT_EQ(solver.prefixesDivBy5(data), result);
}

TEST(BinaryPrefixDivisibleByFive, Fourth)
{
  BinaryPrefixDivisibleByFive::Solution solver;

  vector<int> data = { 1, 1, 1, 0, 1 };
  vector<bool> result = { false, false, false, false, false };

  EXPECT_EQ(solver.prefixesDivBy5(data), result);
}

TEST(BinaryPrefixDivisibleByFive, Fifth)
{
  BinaryPrefixDivisibleByFive::Solution solver;

  vector<int> data = { 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 
    0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 
    0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 
    1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0 };
  vector<bool> result = { false, false, true, false, false, 
    false, false, false, false, false, true, true, true, 
    true, true, true, false, false, false, false, false, 
    false, false, false, false, false, false, false, false, 
    false, false, false, false, false, false, false, false, 
    false, false, false, false, false, false, true, false, 
    false, false, true, false, false, true, false, false, 
    true, true, true, true, true, true, true, false, false, 
    true, false, false, false, false, true, true };

  EXPECT_EQ(solver.prefixesDivBy5(data), result);
}
