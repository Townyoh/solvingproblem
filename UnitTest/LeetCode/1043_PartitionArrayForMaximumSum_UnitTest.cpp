#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1043_PartitionArrayForMaximumSum.cpp"

TEST(PartitionArrayForMaximumSum, First)
{
  vector<int> data{ 1, 15, 7, 9, 2, 5, 10 };
  auto size = 3;
  auto result = 84;

  PartitionArrayForMaximumSum::Solution solver;

  EXPECT_EQ(solver.maxSumAfterPartitioning(data, size), result);
}