#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1034_ColoringABorder.cpp"

TEST(ColoringABorderUnitTest, First)
{
  ColoringABorder::Solution solver;
  vector<vector<int>> maze
  {
    {1, 1, 1},
    {1, 1, 1},
    {1, 1, 1}
  };
  vector<vector<int>> result
  {
    {2, 2, 2 },
    {2, 1, 2 },
    {2, 2, 2 }
  };
  auto r0 = 0;
  auto c0 = 1;
  auto color = 2;

  EXPECT_TRUE(IS_EQUAL_VECTOR(solver.colorBorder(maze, r0, c0, color), result));
}

TEST(ColoringABorderUnitTest, Second)
{
  ColoringABorder::Solution solver;
  vector<vector<int>> maze
  {
    {1, 2, 2},
    {2, 3, 2}
  };
  vector<vector<int>> result
  {
    {1, 3, 3 },
    {2, 3, 3 }
  };
  auto r0 = 0;
  auto c0 = 1;
  auto color = 3;

  EXPECT_TRUE(IS_EQUAL_VECTOR(solver.colorBorder(maze, r0, c0, color), result));
}
