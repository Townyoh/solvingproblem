#include "../pch.h"
#include "../../SolvingProblem/LeetCode/72_EditDistance.cpp"

TEST(EditDistance, First)
{
  auto first = "horse";
  auto second = "ros";
  auto expected = 3;

  EditDistance::Solution solver;

  EXPECT_EQ(solver.minDistance(first, second), expected);
}

TEST(EditDistance, Second)
{
  auto first = "intention";
  auto second = "execution";
  auto expected = 5;

  EditDistance::Solution solver;

  EXPECT_EQ(solver.minDistance(first, second), expected);
}

TEST(EditDistance, Third)
{
  auto first = "";
  auto second = "e";
  auto expected = 1;

  EditDistance::Solution solver;

  EXPECT_EQ(solver.minDistance(first, second), expected);
}
