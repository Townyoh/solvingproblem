#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1026_MaximumDifferenceBetweenNodeAncestor.cpp"

TEST(MaximumDifferenceBetweenNodeAncestor, First)
{
  TreeNode* root = new TreeNode(8);
  root->left = new TreeNode(3);
  root->right = new TreeNode(10);

  root->left->left = new TreeNode(1);
  root->left->right = new TreeNode(6);

  root->right->right = new TreeNode(14);

  root->left->right->left = new TreeNode(4);
  root->left->right->right = new TreeNode(7);

  root->right->right->left = new TreeNode(13);

  MaximumDifferenceBetweenNodeAncestor::Solution solver;

  EXPECT_EQ(solver.maxAncestorDiff(root), 7);
}

TEST(MaximumDifferenceBetweenNodeAncestor, Second)
{
  TreeNode* root = new TreeNode(8);
  root->left = new TreeNode(3);
  root->right = new TreeNode(10);

  root->left->left = new TreeNode(1);
  root->left->right = new TreeNode(6);

  root->right->right = new TreeNode(14);

  root->left->right->left = new TreeNode(40);
  root->left->right->right = new TreeNode(7);

  root->right->right->left = new TreeNode(13);

  MaximumDifferenceBetweenNodeAncestor::Solution solver;

  EXPECT_EQ(solver.maxAncestorDiff(root), 37);
}

