#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1129_ShortestPathWithAlternatingColors.cpp"

TEST(ShortestPathWithAlternatingColors, First)
{
  auto nbNode = 3;
  auto red = vector<vector<int>>
  {
    { 0,1 },
    { 1,2 }
  };
  auto blue = vector<vector<int>>
  {

  };

  auto expected = vector<int>{ 0,1,-1 };

  ShortestPathWithAlternatingColors::Solution solver;

  EXPECT_EQ(solver.shortestAlternatingPaths(nbNode, red, blue), expected);
}

TEST(ShortestPathWithAlternatingColors, Second)
{
  auto nbNode = 3;
  auto red = vector<vector<int>>
  {
    { 0,1 }
  };
  auto blue = vector<vector<int>>
  {
    { 2,1 }
  };

  auto expected = vector<int>{ 0,1,-1 };

  ShortestPathWithAlternatingColors::Solution solver;

  EXPECT_EQ(solver.shortestAlternatingPaths(nbNode, red, blue), expected);
}

TEST(ShortestPathWithAlternatingColors, Third)
{
  auto nbNode = 3;
  auto red = vector<vector<int>>
  {
    { 1,0 }
  };
  auto blue = vector<vector<int>>
  {
    { 2,1 }
  };

  auto expected = vector<int>{ 0,-1,-1 };

  ShortestPathWithAlternatingColors::Solution solver;

  EXPECT_EQ(solver.shortestAlternatingPaths(nbNode, red, blue), expected);
}

TEST(ShortestPathWithAlternatingColors, Fourth)
{
  auto nbNode = 3;
  auto red = vector<vector<int>>
  {
    { 0,1 }
  };
  auto blue = vector<vector<int>>
  {
    { 1,2 }
  };

  auto expected = vector<int>{ 0,1,2 };

  ShortestPathWithAlternatingColors::Solution solver;

  EXPECT_EQ(solver.shortestAlternatingPaths(nbNode, red, blue), expected);
}

TEST(ShortestPathWithAlternatingColors, Fifth)
{
  auto nbNode = 3;
  auto red = vector<vector<int>>
  {
    { 0,1 },
    { 0,2 }
  };
  auto blue = vector<vector<int>>
  {
    { 1,0 }
  };

  auto expected = vector<int>{ 0,1,1 };

  ShortestPathWithAlternatingColors::Solution solver;

  EXPECT_EQ(solver.shortestAlternatingPaths(nbNode, red, blue), expected);
}

TEST(ShortestPathWithAlternatingColors, Sixth)
{
  auto nbNode = 5;
  auto red = vector<vector<int>>
  {
    { 0,1 },
    { 1,2 },
    { 2,3 },
    { 3,4 }
  };
  auto blue = vector<vector<int>>
  {
    { 1,2 },
    { 2,3 },
    { 3,1 }
  };

  auto expected = vector<int>{ 0,1,2,3,7 };

  ShortestPathWithAlternatingColors::Solution solver;

  EXPECT_EQ(solver.shortestAlternatingPaths(nbNode, red, blue), expected);
}

TEST(ShortestPathWithAlternatingColors, Seventh)
{
  auto nbNode = 5;
  auto red = vector<vector<int>>
  {
    { 3, 2 },
    { 4, 1 },
    { 1, 4 },
    { 2, 4 }
  };
  auto blue = vector<vector<int>>
  {
    { 2, 3 },
    { 0, 4 },
    { 4, 3 },
    { 4, 4 },
    { 4, 0 },
    { 1, 0 }
  };

  auto expected = vector<int>{ 0,2,-1,-1,1 };

  ShortestPathWithAlternatingColors::Solution solver;

  EXPECT_EQ(solver.shortestAlternatingPaths(nbNode, red, blue), expected);
}

TEST(ShortestPathWithAlternatingColors, Eighth)
{
  auto nbNode = 5;
  auto red = vector<vector<int>>
  {
    { 2,2 },
    { 0,4 },
    { 4,2 },
    { 4,3 },
    { 2,4 },
    { 0,0 },
    { 0,1 },
    { 2,3 },
    { 1,3 }
  };
  auto blue = vector<vector<int>>
  {
    { 0,4 },
    { 1,0 },
    { 1,4 },
    { 0,0 },
    { 4,0 }
  };

  auto expected = vector<int>{ 0,1,2,2,1 };

  ShortestPathWithAlternatingColors::Solution solver;

  EXPECT_EQ(solver.shortestAlternatingPaths(nbNode, red, blue), expected);
}
