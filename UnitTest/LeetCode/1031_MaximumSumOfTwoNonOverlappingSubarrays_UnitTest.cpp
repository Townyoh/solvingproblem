#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1031_MaximumSumOfTwoNonOverlappingSubarrays.cpp"

TEST(MaximumSumOfTwoNonOverlappingSubarrays, First)
{
  MaximumSumOfTwoNonOverlappingSubarrays::Solution solver;
  vector<int> data{ 0, 6, 5, 2, 2, 5, 1, 9, 4 };

  EXPECT_EQ(solver.maxSumTwoNoOverlap(data, 1, 2), 20);
}

TEST(MaximumSumOfTwoNonOverlappingSubarrays, Second)
{
  MaximumSumOfTwoNonOverlappingSubarrays::Solution solver;
  vector<int> data{ 3, 8, 1, 3, 2, 1, 8, 9, 0 };

  EXPECT_EQ(solver.maxSumTwoNoOverlap(data, 3, 2), 29);
}

TEST(MaximumSumOfTwoNonOverlappingSubarrays, Third)
{
  MaximumSumOfTwoNonOverlappingSubarrays::Solution solver;
  vector<int> data{ 2, 1, 5, 6, 0, 9, 5, 0, 3, 8 };

  EXPECT_EQ(solver.maxSumTwoNoOverlap(data, 4, 3), 31);
}

