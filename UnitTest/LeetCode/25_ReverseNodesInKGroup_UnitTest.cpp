#include "../pch.h"
#include "../../SolvingProblem/LeetCode/25_ReverseNodesInKGroup.cpp"

TEST(ReverseNodesInKGroup, First)
{
  auto root = new ListNode(1);
  {
    root->next = new ListNode(2);
    root->next->next = new ListNode(3);
    root->next->next->next = new ListNode(4);
    root->next->next->next->next = new ListNode(5);
    root->next->next->next->next->next = new ListNode(6);
    root->next->next->next->next->next->next = new ListNode(7);
  }

  auto expected = new ListNode(3);
  {
    expected->next = new ListNode(2);
    expected->next->next = new ListNode(1);
    expected->next->next->next = new ListNode(6);
    expected->next->next->next->next = new ListNode(5);
    expected->next->next->next->next->next = new ListNode(4);
    expected->next->next->next->next->next->next = new ListNode(7);
  }

  auto group = 3;

  ReverseNodesInKGroup::Solution solver;
  EXPECT_TRUE(IS_EQUAL_LINKED_LIST(solver.reverseKGroup(root, group), expected));
}

TEST(ReverseNodesInKGroup, Second)
{
  auto root = new ListNode(1);
  {
    root->next = new ListNode(2);
    root->next->next = new ListNode(3);
    root->next->next->next = new ListNode(4);
    root->next->next->next->next = new ListNode(5);
  }

  auto expected = new ListNode(2);
  {
    expected->next = new ListNode(1);
    expected->next->next = new ListNode(4);
    expected->next->next->next = new ListNode(3);
    expected->next->next->next->next = new ListNode(5);
  }

  auto group = 2;

  ReverseNodesInKGroup::Solution solver;
  EXPECT_TRUE(IS_EQUAL_LINKED_LIST(solver.reverseKGroup(root, group), expected));
}