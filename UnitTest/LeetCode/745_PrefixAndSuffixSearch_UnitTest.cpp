#include "../pch.h"
#include "../../SolvingProblem/LeetCode/745_PrefixAndSuffixSearch.cpp"

TEST(PrefixAndSuffixSearch, First)
{
  auto words = vector<string>{ "apple" };
  auto input = vector<vector<string>>
  {
    {"a", "e"},
    {"b", ""},
  };
  auto expected = vector<int>{ 0, -1 };

  PrefixAndSuffixSearch::WordFilter solver(words);

  for (auto index = 0; index < input.size(); index++)
  {
    EXPECT_EQ(solver.f(input[index][0], input[index][1]), expected[index]);
  }
}

TEST(PrefixAndSuffixSearch, Second)
{
  auto words = vector<string>{ "abbbababbb","baaabbabbb","abababbaaa","abbbbbbbba","bbbaabbbaa","ababbaabaa","baaaaabbbb","babbabbabb","ababaababb","bbabbababa" };
  auto input = vector<vector<string>>
  {
    { "","abaa"},
    { "babbab",""},
    { "ab","baaa"},
    { "baaabba","b"},
    { "abab","abbaabaa"},
    { "","aa"},
    { "","bba"},
    { "","baaaaabbbb"},
    { "ba","aabbbb"},
    { "baaa","aabbabbb"}
  };
  auto expected = vector<int>{ 5,7,2,1,5,5,3,6,6,1 };

  PrefixAndSuffixSearch::WordFilter solver(words);

  for (auto index = 0; index < input.size(); index++)
  {
    EXPECT_EQ(solver.f(input[index][0], input[index][1]), expected[index]);
  }
}
