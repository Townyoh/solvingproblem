#include "../pch.h"
#include "../../SolvingProblem/LeetCode/76_MinimumWindowSubstring.cpp"

TEST(MinimumWindowSubstring, First)
{
  auto reference = string("ADOBECODEBANC");
  auto word = string("ABC");

  auto expected = string("BANC");

  MinimumWindowSubstring::Solution solver;

  EXPECT_EQ(solver.minWindow(reference, word), expected);
}

TEST(MinimumWindowSubstring, Second)
{
  auto reference = string("AD");
  auto word = string("A");

  auto expected = string("A");

  MinimumWindowSubstring::Solution solver;

  EXPECT_EQ(solver.minWindow(reference, word), expected);
}

TEST(MinimumWindowSubstring, Thrid)
{
  auto reference = string("A");
  auto word = string("A");

  auto expected = string("A");

  MinimumWindowSubstring::Solution solver;

  EXPECT_EQ(solver.minWindow(reference, word), expected);
}

TEST(MinimumWindowSubstring, Fourth)
{
  auto reference = string("AA");
  auto word = string("AA");

  auto expected = string("AA");

  MinimumWindowSubstring::Solution solver;

  EXPECT_EQ(solver.minWindow(reference, word), expected);
}

TEST(MinimumWindowSubstring, Fifth)
{
  auto reference = string("bba");
  auto word = string("ab");

  auto expected = string("ba");

  MinimumWindowSubstring::Solution solver;

  EXPECT_EQ(solver.minWindow(reference, word), expected);
}
