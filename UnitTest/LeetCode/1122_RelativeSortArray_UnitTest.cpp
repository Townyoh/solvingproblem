#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1122_RelativeSortArray.cpp"

TEST(RelativeSortArray_UnitTest, First)
{
  auto inputArr1 = vector<int>{ 2,3,1,3,2,4,6,7,9,2,19 };
  auto inputArr2 = vector<int>{ 2,1,4,3,9,6 };

  auto output = vector<int>{ 2,2,2,1,4,3,3,9,6,7,19 };

  RelativeSortArray::Solution solver;

  EXPECT_EQ(solver.relativeSortArray(inputArr1, inputArr2), output);
}
