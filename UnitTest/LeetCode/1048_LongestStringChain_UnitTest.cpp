#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1048_LongestStringChain.cpp"

TEST(LongestStringChain, First)
{
  auto data = vector<string>{ "a", "b", "ba", "bca", "bda", "bdca" };
  auto result = 4;

  LongestStringChain::Solution solver;

  EXPECT_EQ(solver.longestStrChain(data), result);
}