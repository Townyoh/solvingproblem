#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1035_UncrossedLines.cpp"

TEST(UncrossedLinesUnitTest, First)
{
  UncrossedLines::Solution solver;
  vector<int> first{ 2, 5, 1, 2, 5 };
  vector<int> second{ 10, 5, 2, 1, 5, 2 };

  EXPECT_EQ(solver.maxUncrossedLines(first, second), 3);
}

TEST(UncrossedLinesUnitTest, Second)
{
  UncrossedLines::Solution solver;
  vector<int> first{ 1, 3, 7, 1, 7, 5 };
  vector<int> second{ 1, 9, 2, 5, 1 };

  EXPECT_EQ(solver.maxUncrossedLines(first, second), 2);
}

TEST(UncrossedLinesUnitTest, Third)
{
  UncrossedLines::Solution solver;
  vector<int> first{ 10, 5, 2, 1, 5 };
  vector<int> second{ 2, 5, 3, 2, 5 };

  EXPECT_EQ(solver.maxUncrossedLines(first, second), 3);
}

