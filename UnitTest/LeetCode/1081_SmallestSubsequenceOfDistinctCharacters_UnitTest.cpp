#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1081_SmallestSubsequenceOfDistinctCharacters.cpp"

TEST(SmallestSubsequenceOfDistinctCharacters, First)
{
  auto text = string("cdadabcc");
  auto result = string("adbc");

  SmallestSubsequenceOfDistinctCharacters::Solution solver;

  EXPECT_EQ(solver.smallestSubsequence(text), result);
}

TEST(SmallestSubsequenceOfDistinctCharacters, Second)
{
  auto text = string("abcd");
  auto result = string("abcd");

  SmallestSubsequenceOfDistinctCharacters::Solution solver;

  EXPECT_EQ(solver.smallestSubsequence(text), result);
}

TEST(SmallestSubsequenceOfDistinctCharacters, Third)
{
  auto text = string("ecbacba");
  auto result = string("eacb");

  SmallestSubsequenceOfDistinctCharacters::Solution solver;

  EXPECT_EQ(solver.smallestSubsequence(text), result);
}

TEST(SmallestSubsequenceOfDistinctCharacters, Fourth)
{
  auto text = string("leetcode");
  auto result = string("letcod");

  SmallestSubsequenceOfDistinctCharacters::Solution solver;

  EXPECT_EQ(solver.smallestSubsequence(text), result);
}

TEST(SmallestSubsequenceOfDistinctCharacters, Fifth)
{
  auto text = string("ddeeeccdce");
  auto result = string("cde");

  SmallestSubsequenceOfDistinctCharacters::Solution solver;

  EXPECT_EQ(solver.smallestSubsequence(text), result);
}

