#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1027_LongestArithmeticSequence.cpp"

TEST(LongestArithmeticSequence, First)
{
  LongestArithmeticSequence::Solution solver;
  vector<int> data{ 3,  6,  9,  12 };

  EXPECT_EQ(solver.longestArithSeqLength(data),  4);
}

TEST(LongestArithmeticSequence, Second)
{
  LongestArithmeticSequence::Solution solver;
  vector<int> data{ 9,  4,  7,  2,  10 };

  EXPECT_EQ(solver.longestArithSeqLength(data),  3);
}

TEST(LongestArithmeticSequence, Third)
{
  LongestArithmeticSequence::Solution solver;
  vector<int> data{ 20,  1,  15,  3,  10,  5,  8 };

  EXPECT_EQ(solver.longestArithSeqLength(data),  4);
}

TEST(LongestArithmeticSequence, Fourth)
{
  LongestArithmeticSequence::Solution solver;
  vector<int> data{ 12,  28,  13,  6,  34,  36,  53,  24,  29,  2,  23,  0,  22, 
    25,  53,  34,  23,  50,  35,  43,  53,  11,  48,  56,  44,  53,  31,  6,  31,  57, 
    46,  6,  17,  42,  48,  28,  5,  24,  0,  14,  43,  12,  21,  6,  30,  37,  16,  56,  19, 
    45,  51,  10,  22,  38,  39,  23,  8,  29,  60,  18 };

  EXPECT_EQ(solver.longestArithSeqLength(data),  4);
}

TEST(LongestArithmeticSequence, Fifth)
{
  LongestArithmeticSequence::Solution solver;
  vector<int> data{ 22, 8, 57, 41, 36, 46, 42, 28, 42, 14,
    9, 43, 27, 51, 0, 0, 38, 50, 31, 60,
    29, 31, 20, 23, 37, 53, 27, 1, 47, 42,
    28, 31, 10, 35, 39, 12, 15, 6, 35, 31,
    45, 21, 30, 19, 5, 5, 4, 18, 38, 51, 
    10, 7, 20, 38, 28, 53, 15, 55, 60, 56, 
    43, 48, 34, 53, 54, 55, 14, 9, 56, 52 };

  EXPECT_EQ(solver.longestArithSeqLength(data),  6);
}

