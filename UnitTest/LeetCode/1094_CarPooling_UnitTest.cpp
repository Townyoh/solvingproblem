#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1094_CarPooling.cpp"

TEST(CarPooling, First)
{
  auto data = vector<vector<int>>
  {
    { 2,1,5 },
    { 3,3,7 }
  };
  auto capacity = 4;

  CarPooling::Solution solver;

  EXPECT_FALSE(solver.carPooling(data, capacity));
}

TEST(CarPooling, Second)
{
  auto data = vector<vector<int>>
  {
    { 2,1,5 },
    { 3,3,7 }
  };
  auto capacity = 5;

  CarPooling::Solution solver;

  EXPECT_TRUE(solver.carPooling(data, capacity));
}

TEST(CarPooling, Third)
{
  auto data = vector<vector<int>>
  {
    { 2,1,5 },
    { 3,5,7 }
  };
  auto capacity = 3;

  CarPooling::Solution solver;

  EXPECT_TRUE(solver.carPooling(data, capacity));
}

TEST(CarPooling, Fourth)
{
  auto data = vector<vector<int>>
  {
    { 3,2,7 },
    { 3,7,9 },
    { 8,3,9 }
  };       
  auto capacity = 11;

  CarPooling::Solution solver;

  EXPECT_TRUE(solver.carPooling(data, capacity));
}

