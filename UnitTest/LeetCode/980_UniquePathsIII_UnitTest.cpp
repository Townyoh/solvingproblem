#include "../pch.h"
#include "../../SolvingProblem/LeetCode/980_UniquePathsIII.cpp"

TEST(UniquePathsIII, First)
{
  auto graph = vector<vector<int>>
  {
    { 1,0,0,0 },
    { 0,0,0,0 },
    { 0,0,2,-1 }
  };
  auto expected = 2;

  UniquePathsIII::Solution solver;

  EXPECT_EQ(solver.uniquePathsIII(graph), expected);
}

TEST(UniquePathsIII, Second)
{
  auto graph = vector<vector<int>>
  {
    { 1,0,0,0 },
    { 0,0,0,0 },
    { 0,0,0,2 }
  };
  auto expected = 4;

  UniquePathsIII::Solution solver;

  EXPECT_EQ(solver.uniquePathsIII(graph), expected);
}

TEST(UniquePathsIII, Third)
{
  auto graph = vector<vector<int>>
  {
    { 0,1 },
    { 2,0 }
  };
  auto expected = 0;

  UniquePathsIII::Solution solver;

  EXPECT_EQ(solver.uniquePathsIII(graph), expected);
}
