#include "../pch.h"
#include "../../SolvingProblem/LeetCode/212_WordSearchII.cpp"

TEST(WordSearchII, First)
{
  auto board = vector<vector<char>>
  {
    { 'o','a','a','n' },
    { 'e','t','a','e' },
    { 'i','h','k','r' },
    { 'i','f','l','v' }
  };
  auto words = vector<string> { "oath","pea","eat","rain" };
  auto expected = vector<string>{ "oath", "eat" };

  WordSearchII::Solution solver;
  EXPECT_EQ(solver.findWords(board, words), expected);
}

TEST(WordSearchII, Second)
{
  auto board = vector<vector<char>>
  {
    { 'b' },
    { 'a' },
    { 'b' },
    { 'b' },
    { 'a' }
  };
  auto words = vector<string>{ "baa", "abba", "baab", "aba" };
  auto expected = vector<string>{ "abba" };

  WordSearchII::Solution solver;
  EXPECT_EQ(solver.findWords(board, words), expected);
}

TEST(WordSearchII, Third)
{
  auto board = vector<vector<char>>
  {
    { 'a', 'b' },
    { 'c', 'd' }
  };
  auto words = vector<string>{ "ab", "cb", "ad", "bd", "ac", "ca", "da", "bc", "db", "adcb", "dabc", "abb", "acb" };
  auto expected = vector<string>{ "ab","ac","bd","ca","db" };

  WordSearchII::Solution solver;
  EXPECT_EQ(solver.findWords(board, words), expected);
}

