#include "../pch.h"
#include "../../SolvingProblem/LeetCode/122_BestTimeToBuyAndSellStockII.cpp"

TEST(BestTimeToBuyAndSellStockII, First)
{
  auto prices = vector<int>{ 7,1,5,3,6,4 };
  auto expected = 7;

  BestTimeToBuyAndSellStockII::Solution solver;

  EXPECT_EQ(solver.maxProfit(prices), expected);
}

TEST(BestTimeToBuyAndSellStockII, Second)
{
  auto prices = vector<int>{ 1,2,3,4,5 };
  auto expected = 4;

  BestTimeToBuyAndSellStockII::Solution solver;

  EXPECT_EQ(solver.maxProfit(prices), expected);
}

