#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1052_GrumpyBookstoreOwner.cpp"

TEST(GrumpyBookstoreOwner, First)
{
  auto customers = vector<int>{ 1, 0, 1, 2, 1, 1, 7, 5 };
  auto grumpy = vector<int>{ 0, 1, 0, 1, 0, 1, 0, 1 };
  auto result = 16;
  auto joker = 3;

  GrumpyBookstoreOwner::Solution solver;

  EXPECT_EQ(solver.maxSatisfied(customers, grumpy, joker), result);
}