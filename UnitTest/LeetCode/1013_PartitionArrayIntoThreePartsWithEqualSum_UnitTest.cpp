#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1013_PartitionArrayIntoThreePartsWithEqualSum.cpp"

TEST(PartitionArrayIntoThreePartsWithEqualSum, First)
{
  auto data = vector<int>{ 0,2,1,-6,6,-7,9,1,2,0,1 };
  PartitionArrayIntoThreePartsWithEqualSum::Solution solver;

  EXPECT_TRUE(solver.canThreePartsEqualSum(data));
}

TEST(PartitionArrayIntoThreePartsWithEqualSum, Second)
{
  auto data = vector<int>{ 0,2,1,-6,6,7,9,-1,2,0,1 };
  PartitionArrayIntoThreePartsWithEqualSum::Solution solver;

  EXPECT_FALSE(solver.canThreePartsEqualSum(data));
}

TEST(PartitionArrayIntoThreePartsWithEqualSum, Third)
{
  auto data = vector<int>{ 3,3,6,5,-2,2,5,1,-9,4 };
  PartitionArrayIntoThreePartsWithEqualSum::Solution solver;

  EXPECT_TRUE(solver.canThreePartsEqualSum(data));
}