#include "../pch.h"
#include "../../SolvingProblem/LeetCode/174_DungeonGame.cpp"

TEST(DungeonGame, First)
{
  auto dungeon = vector<vector<int>>
  {
    { -2, -3, 3 },
    { -5, -10, 1 },
    { 10, 30, -5 }
  };
  auto expected = 7;

  DungeonGame::Solution solver;

  EXPECT_EQ(solver.calculateMinimumHP(dungeon), expected);
}