#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1032_StreamOfCharacters.cpp" 

TEST(StreamOfCharactersUnitTest, First)
{
  auto data = vector<string>{ "cd", "f", "kl" };
  StreamOfCharacters::StreamChecker streamChecker(data);

  EXPECT_FALSE(streamChecker.query('a')); 
  EXPECT_FALSE(streamChecker.query('b')); 
  EXPECT_FALSE(streamChecker.query('c')); 
  EXPECT_TRUE(streamChecker.query('d'));  
  EXPECT_FALSE(streamChecker.query('e')); 
  EXPECT_TRUE(streamChecker.query('f'));  
  EXPECT_FALSE(streamChecker.query('g')); 
  EXPECT_FALSE(streamChecker.query('h')); 
  EXPECT_FALSE(streamChecker.query('i')); 
  EXPECT_FALSE(streamChecker.query('j')); 
  EXPECT_FALSE(streamChecker.query('k')); 
  EXPECT_TRUE(streamChecker.query('l'));
}

TEST(StreamOfCharactersUnitTest, Second)
{
  auto data = vector<string>{ "ab","ba","aaab","abab","baa" };
  StreamOfCharacters::StreamChecker streamChecker(data);

  EXPECT_FALSE(streamChecker.query('a'));
  EXPECT_FALSE(streamChecker.query('a'));
  EXPECT_FALSE(streamChecker.query('a'));
  EXPECT_FALSE(streamChecker.query('a'));
  EXPECT_FALSE(streamChecker.query('a'));
  EXPECT_TRUE(streamChecker.query('b'));
  EXPECT_TRUE(streamChecker.query('a'));
  EXPECT_TRUE(streamChecker.query('b'));
  EXPECT_TRUE(streamChecker.query('a'));
  EXPECT_TRUE(streamChecker.query('b'));
  EXPECT_FALSE(streamChecker.query('b'));
  EXPECT_FALSE(streamChecker.query('b'));
  EXPECT_TRUE(streamChecker.query('a'));
  EXPECT_TRUE(streamChecker.query('b'));
  EXPECT_TRUE(streamChecker.query('a'));
  EXPECT_TRUE(streamChecker.query('b'));
  EXPECT_FALSE(streamChecker.query('b'));
  EXPECT_FALSE(streamChecker.query('b'));
  EXPECT_FALSE(streamChecker.query('b'));
  EXPECT_TRUE(streamChecker.query('a'));
  EXPECT_TRUE(streamChecker.query('b'));
  EXPECT_TRUE(streamChecker.query('a'));
  EXPECT_TRUE(streamChecker.query('b'));
  EXPECT_TRUE(streamChecker.query('a'));
  EXPECT_TRUE(streamChecker.query('a'));
  EXPECT_FALSE(streamChecker.query('a'));
  EXPECT_TRUE(streamChecker.query('b'));
  EXPECT_TRUE(streamChecker.query('a'));
  EXPECT_TRUE(streamChecker.query('a'));
  EXPECT_FALSE(streamChecker.query('a'));
}