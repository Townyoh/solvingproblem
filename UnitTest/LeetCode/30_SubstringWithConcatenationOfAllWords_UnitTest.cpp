#include "../pch.h"
#include "../../SolvingProblem/LeetCode/30_SubstringWithConcatenationOfAllWords.cpp"

TEST(SubstringWithConcatenationOfAllWords_UnitTest, First)
{
  auto input = string("barfoothefoobarman");
  auto words = vector<string>{ "foo","bar" };
  auto expected = vector<int>{ 0, 9 };

  SubstringWithConcatenationOfAllWords::Solution solver;

  EXPECT_EQ(solver.findSubstring(input, words), expected);
}

TEST(SubstringWithConcatenationOfAllWords_UnitTest, Second)
{
  auto input = string("wordgoodgoodgoodbestword");
  auto words = vector<string>{ "word","good","best","word" };
  auto expected = vector<int>{  };

  SubstringWithConcatenationOfAllWords::Solution solver;

  EXPECT_EQ(solver.findSubstring(input, words), expected);
}

TEST(SubstringWithConcatenationOfAllWords_UnitTest, Third)
{
  auto input = string("aaa");
  auto words = vector<string>{ "a","a" };
  auto expected = vector<int>{ 0, 1 };

  SubstringWithConcatenationOfAllWords::Solution solver;

  EXPECT_EQ(solver.findSubstring(input, words), expected);
}

