#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1331_RankTransformOfAnArray.cpp"

TEST(RankTransformOfAnArray, First)
{
  auto input = vector<int>{ 40,10,20,30 };
  auto expected = vector<int>{ 4,1,2,3 };

  RankTransformOfAnArray::Solution solver;

  EXPECT_EQ(solver.arrayRankTransform(input), expected);
}

TEST(RankTransformOfAnArray, Second)
{
  auto input = vector<int>{ 100,100,100 };
  auto expected = vector<int>{ 1,1,1 };

  RankTransformOfAnArray::Solution solver;

  EXPECT_EQ(solver.arrayRankTransform(input), expected);
}

TEST(RankTransformOfAnArray, Third)
{
  auto input = vector<int>{ 37,12,28,9,100,56,80,5,12 };
  auto expected = vector<int>{ 5,3,4,2,8,6,7,1,3 };

  RankTransformOfAnArray::Solution solver;

  EXPECT_EQ(solver.arrayRankTransform(input), expected);
}
