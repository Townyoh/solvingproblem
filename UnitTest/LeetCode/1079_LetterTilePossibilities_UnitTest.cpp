#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1079_LetterTilePossibilities.cpp"

TEST(LetterTilePossibilities, First)
{
  auto data = string("AAB");
  auto result = 8;

  LetterTilePossibilities::Solution solver;

  EXPECT_EQ(solver.numTilePossibilities(data), result);
}

TEST(LetterTilePossibilities, Second)
{
  auto data = string("AAABBC");
  auto result = 188;

  LetterTilePossibilities::Solution solver;

  EXPECT_EQ(solver.numTilePossibilities(data), result);
}
