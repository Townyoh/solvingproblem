#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1072_FlipColumnsForMaximumNumberOfEqualRows.cpp"

TEST(FlipColumnsForMaximumNumberOfEqualRows, First)
{
  auto matrix = vector<vector<int>>
  {
    { 0, 0, 0 }, 
    { 0, 0, 1 }, 
    { 1, 1, 0 }
  };
  auto result = 2;

  FlipColumnsForMaximumNumberOfEqualRows::Solution solver;

  EXPECT_EQ(solver.maxEqualRowsAfterFlips(matrix), result);
}
