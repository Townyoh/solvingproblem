#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1021_RemoveOUtermostParentheses.cpp"

TEST(RemoveOUtermostParentheses, First)
{
  RemoveOutermostParentheses::Solution solver;
  string data = { "(()())(())" };
  string result = { "()()()" };

  EXPECT_EQ(solver.removeOuterParentheses(data), result);
}

TEST(RemoveOUtermostParentheses, Second)
{
  RemoveOutermostParentheses::Solution solver;
  string data = { "(()())(())(()(()))" };
  string result = { "()()()()(())" };

  EXPECT_EQ(solver.removeOuterParentheses(data), result);
}

TEST(RemoveOUtermostParentheses, Third)
{
  RemoveOutermostParentheses::Solution solver;
  string data = { "()()" };
  string result = { "" };

  EXPECT_EQ(solver.removeOuterParentheses(data), result);
}
