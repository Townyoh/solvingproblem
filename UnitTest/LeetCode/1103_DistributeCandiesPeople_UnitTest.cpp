#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1103_DistributeCandiesPeople.cpp"

TEST(DistributeCandiesPeople, First)
{
  auto candies = 7;
  auto child = 4;
  auto result = vector<int>{ 1,2,3,1 };

  DistributeCandiesPeople::Solution solver;

  EXPECT_EQ(solver.distributeCandies(candies, child), result);
}

TEST(DistributeCandiesPeople, Second)
{
  auto candies = 10;
  auto child = 3;
  auto result = vector<int>{ 5,2,3 };

  DistributeCandiesPeople::Solution solver;

  EXPECT_EQ(solver.distributeCandies(candies, child), result);
}
