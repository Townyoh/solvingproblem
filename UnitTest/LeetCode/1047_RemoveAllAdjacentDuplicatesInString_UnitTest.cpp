#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1047_RemoveAllAdjacentDuplicatesInString.cpp"

TEST(RemoveAllAdjacentDuplicatesInString, First)
{
  auto data = string{ "abbaca" };
  auto result = string{ "ca" };

  RemoveAllAdjacentDuplicatesInString::Solution solver;
  EXPECT_EQ(solver.removeDuplicates(data), result);
}