#include "../pch.h"
#include "../../SolvingProblem/LeetCode/23_MergeKSortedLists.cpp"

TEST(MergeKSortedLists, First)
{
  auto root1 = new ListNode(1);
  {
    root1->next = new ListNode(4);
    root1->next->next = new ListNode(5);
  }

  auto root2 = new ListNode(1);
  {
    root2->next = new ListNode(3);
    root2->next->next = new ListNode(4);
  }

  auto root3 = new ListNode(2);
  {
    root3->next = new ListNode(6);
  }

  auto expected = new ListNode(1);
  {
    expected->next = new ListNode(1);
    expected->next->next = new ListNode(2);
    expected->next->next->next = new ListNode(3);
    expected->next->next->next->next = new ListNode(4);
    expected->next->next->next->next->next = new ListNode(4);
    expected->next->next->next->next->next->next = new ListNode(5);
    expected->next->next->next->next->next->next->next = new ListNode(6);
  }

  auto lists = vector<ListNode*>{ root1, root2, root3 };

  MergeKSortedLists::Solution solver;
  EXPECT_TRUE(IS_EQUAL_LINKED_LIST(solver.mergeKLists(lists), expected));
}

TEST(MergeKSortedLists, Second)
{
  auto root1 = new ListNode(2);

  auto root2 = nullptr;

  auto root3 = new ListNode(-1);

  auto expected = new ListNode(-1);
  {
    expected->next = new ListNode(2);
  }

  auto lists = vector<ListNode*>{ root1, root2, root3 };

  MergeKSortedLists::Solution solver;
  EXPECT_TRUE(IS_EQUAL_LINKED_LIST(solver.mergeKLists(lists), expected));
}