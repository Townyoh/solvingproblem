#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1090_LargestValuesFromLabels.cpp"

TEST(LargestValuesFromLabels, First)
{
  auto value = vector<int>{ 5,4,3,2,1 };
  auto label = vector<int>{ 1,1,2,2,3 };
  auto wanted = 3;
  auto limit = 1;
  auto result = 9;

  LargestValuesFromLabels::Solution solver;

  EXPECT_EQ(solver.largestValsFromLabels(value, label, wanted, limit), result);
}

TEST(LargestValuesFromLabels, Second)
{
  auto value = vector<int>{ 5,4,3,2,1 };
  auto label = vector<int>{ 1,3,3,3,2 };
  auto wanted = 3;
  auto limit = 2;
  auto result = 12;

  LargestValuesFromLabels::Solution solver;

  EXPECT_EQ(solver.largestValsFromLabels(value, label, wanted, limit), result);
}

TEST(LargestValuesFromLabels, Third)
{
  auto value = vector<int>{ 9,8,8,7,6 };
  auto label = vector<int>{ 0,0,0,1,1 };
  auto wanted = 3;
  auto limit = 1;
  auto result = 16;

  LargestValuesFromLabels::Solution solver;

  EXPECT_EQ(solver.largestValsFromLabels(value, label, wanted, limit), result);
}

TEST(LargestValuesFromLabels, Fourth)
{
  auto value = vector<int>{ 9,8,8,7,6 };
  auto label = vector<int>{ 0,0,0,1,1 };
  auto wanted = 3;
  auto limit = 2;
  auto result = 24;

  LargestValuesFromLabels::Solution solver;

  EXPECT_EQ(solver.largestValsFromLabels(value, label, wanted, limit), result);
}

