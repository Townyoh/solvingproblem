#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1022_SumOfRootToLeafBinaryNumbers.cpp"

TEST(SumOfRootToLeafBinaryNumbers, First)
{
  TreeNode* root = new TreeNode(1);

  root->left = new TreeNode(0);
  root->right = new TreeNode(1);

  root->left->left = new TreeNode(0);
  root->left->right = new TreeNode(1);

  root->right->left = new TreeNode(0);
  root->right->right = new TreeNode(1);

  SumOfRootToLeafBinaryNumbers::Solution solver;

  EXPECT_EQ(solver.sumRootToLeaf(root), 22);
}