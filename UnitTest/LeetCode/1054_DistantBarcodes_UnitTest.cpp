#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1054_DistantBarcodes.cpp"

TEST(DistantBarcodes, First)
{
  auto data = vector<int>{ 1, 1, 1, 2, 2, 2 };
  auto result = vector<int>{ 1, 2, 1, 2, 1, 2 };

  DistantBarcodes::Solution solver;

  EXPECT_EQ(solver.rearrangeBarcodes(data), result);
}
