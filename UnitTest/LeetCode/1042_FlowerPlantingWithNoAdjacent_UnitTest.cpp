#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1042_FlowerPlantingWithNoAdjacent.cpp"

TEST(FlowerPlantingWithNoAdjacent, First)
{
  auto input = 3;
  vector<vector<int>> data 
  {
    { 1, 2 },
    { 2, 3 },
    { 3, 1 }
  };
  vector<int> result{ 1, 2, 3 };

  FlowerPlantingWithNoAdjacent::Solution solver;

  EXPECT_EQ(solver.gardenNoAdj(input, data), result);
}

TEST(FlowerPlantingWithNoAdjacent, Second)
{
  auto input = 4;
  vector<vector<int>> data
  {
    { 1, 2 },
    { 3, 4 }
  };
  vector<int> result{ 1, 2, 1, 2 };

  FlowerPlantingWithNoAdjacent::Solution solver;

  EXPECT_EQ(solver.gardenNoAdj(input, data), result);
}

TEST(FlowerPlantingWithNoAdjacent, Third)
{
  auto input = 4;
  vector<vector<int>> data
  {
    { 1, 2 },
    { 2, 3 },
    { 3, 4 },
    { 4, 1 },
    { 1, 3 },
    { 2, 4 }
  };
  vector<int> result{ 1, 2, 3, 4 };

  FlowerPlantingWithNoAdjacent::Solution solver;

  EXPECT_EQ(solver.gardenNoAdj(input, data), result);
}

