#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1014_BestSightseeingPair.cpp"

TEST(BestSightseeingPair, First)
{
  auto data = vector<int>{ 8,1,5,2,6 };
  auto result = 11;

  BestSightseeingPair::Solution solver;

  EXPECT_EQ(solver.maxScoreSightseeingPair(data), result);
}