#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1171_RemoveZeroSumConsecutiveNodesFromLinkedList.cpp"

TEST(RemoveZeroSumConsecutiveNodesFromLinkedList, First)
{
  auto head = new ListNode(1);
  {
    head->next = new ListNode(2);
    head->next->next = new ListNode(-3);
    head->next->next->next = new ListNode(3);
    head->next->next->next->next = new ListNode(1);
  }

  auto expected = new ListNode(3);
  {
    expected->next = new ListNode(1);
  }

  RemoveZeroSumConsecutiveNodesFromLinkedList::Solution solver;

  EXPECT_TRUE(IS_EQUAL_LINKED_LIST(solver.removeZeroSumSublists(head), expected));
}

TEST(RemoveZeroSumConsecutiveNodesFromLinkedList, Second)
{
  auto head = new ListNode(1);
  {
    head->next = new ListNode(2);
    head->next->next = new ListNode(3);
    head->next->next->next = new ListNode(-3);
    head->next->next->next->next = new ListNode(4);
  }

  auto expected = new ListNode(1);
  {
    expected->next = new ListNode(2);
    expected->next->next = new ListNode(4);
  }

  RemoveZeroSumConsecutiveNodesFromLinkedList::Solution solver;

  EXPECT_TRUE(IS_EQUAL_LINKED_LIST(solver.removeZeroSumSublists(head), expected));
}

TEST(RemoveZeroSumConsecutiveNodesFromLinkedList, Third)
{
  auto head = new ListNode(1);
  {
    head->next = new ListNode(2);
    head->next->next = new ListNode(3);
    head->next->next->next = new ListNode(-3);
    head->next->next->next->next = new ListNode(-2);
  }

  auto expected = new ListNode(1);
  {
  }

  RemoveZeroSumConsecutiveNodesFromLinkedList::Solution solver;

  EXPECT_TRUE(IS_EQUAL_LINKED_LIST(solver.removeZeroSumSublists(head), expected));
}
