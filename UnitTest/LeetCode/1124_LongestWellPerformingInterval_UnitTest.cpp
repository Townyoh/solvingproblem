#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1124_LongestWellPerformingInterval.cpp"

TEST(LongestWellPerformingInterval_UnitTest, First)
{
  auto hours = vector<int>{ 9,9,6,0,6,6,9 };
  auto result = 3;

  LongestWellPerformingInterval::Solution solver;
  
  EXPECT_EQ(solver.longestWPI(hours), result);
}

TEST(LongestWellPerformingInterval_UnitTest, Second)
{
  auto hours = vector<int>{ 9,9,6,0,6,9,9 };
  auto result = 7;

  LongestWellPerformingInterval::Solution solver;

  EXPECT_EQ(solver.longestWPI(hours), result);
}

TEST(LongestWellPerformingInterval_UnitTest, Third)
{
  auto hours = vector<int>{ 9,9,0,0,0,0,0,0,0,9,9,9 };
  auto result = 5;

  LongestWellPerformingInterval::Solution solver;

  EXPECT_EQ(solver.longestWPI(hours), result);
}