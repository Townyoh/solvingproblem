#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1078_OccurrencesAfterBigram.cpp"

TEST(OccurrencesAfterBigram, First)
{
  auto text = string("alice is a good girl she is a good student");
  auto first = string("a");
  auto second = string("good");

  auto result = vector<string>
  {
    "girl",
    "student"
  };

  OccurrencesAfterBigram::Solution solver;

  EXPECT_EQ(solver.findOcurrences(text, first, second), result);
}

TEST(OccurrencesAfterBigram, Second)
{
  auto text = string("we will we will rock you");
  auto first = string("we");
  auto second = string("will");

  auto result = vector<string>
  {
    "we",
    "rock"
  };

  OccurrencesAfterBigram::Solution solver;

  EXPECT_EQ(solver.findOcurrences(text, first, second), result);
}

