#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1023_CamelcaseMatching.cpp"

TEST(CamelcaseMatchingUnitTest, First)
{
  CamelcaseMatching::Solution solver;
  vector<string> data
  {
    "FooBar",
    "FooBarTest",
    "FootBall",
    "FrameBuffer",
    "ForceFeedBack"
  };
  string pattern = "FB";

  vector<bool> result
  {
    true,
    false,
    true,
    true,
    false
  };

  EXPECT_EQ(solver.camelMatch(data, pattern), result);
}

TEST(CamelcaseMatchingUnitTest, Second)
{
  CamelcaseMatching::Solution solver;
  vector<string> data
  {
    "FooBar",
    "FooBarTest",
    "FootBall",
    "FrameBuffer",
    "ForceFeedBack"
  };
  string pattern = "FoBa";

  vector<bool> result
  {
    true,
    false,
    true,
    false,
    false
  };

  EXPECT_EQ(solver.camelMatch(data, pattern), result);
}

TEST(CamelcaseMatchingUnitTest, Third)
{
  CamelcaseMatching::Solution solver;
  vector<string> data
  {
    "FooBar",
    "FooBarTest",
    "FootBall",
    "FrameBuffer",
    "ForceFeedBack"
  };
  string pattern = "FoBaT";

  vector<bool> result
  {
    false,
    true,
    false,
    false,
    false
  };

  EXPECT_EQ(solver.camelMatch(data, pattern), result);
}

TEST(CamelcaseMatchingUnitTest, Fourth)
{
  CamelcaseMatching::Solution solver;
  vector<string> data
  {
    "CompetitiveProgramming",
    "CounterPick",
    "ControlPanel"
  };
  string pattern = "CooP";

  vector<bool> result
  {
    false,
    false,
    true
  };

  EXPECT_EQ(solver.camelMatch(data, pattern), result);
}

