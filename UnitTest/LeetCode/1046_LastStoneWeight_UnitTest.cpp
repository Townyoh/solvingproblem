#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1046_LAstStoneWeight.cpp"

TEST(LastStoneWeight, First)
{
  auto data = vector<int>{ 2, 7, 4, 1, 8, 1 };
  auto result = 1;

  LastStoneWeight::Solution solver;

  EXPECT_EQ(solver.lastStoneWeight(data), result);
}