#include "../pch.h"
#include "../../SolvingProblem/LeetCode/692_TopKFrequentWords.cpp"

TEST(TopKFrequentWords, First)
{
  auto input = vector<string>{ "i", "love", "leetcode", "i", "love", "coding" };
  auto expected = vector<string>{ "i", "love" };

  auto k = 2;

  TopKFrequentWords::Solution solver;
  EXPECT_EQ(solver.topKFrequent(input, k), expected);
}

TEST(TopKFrequentWords, Second)
{
  auto input = vector<string>{ "the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is" };
  auto expected = vector<string>{ "the", "is", "sunny", "day" };

  auto k = 4;

  TopKFrequentWords::Solution solver;
  EXPECT_EQ(solver.topKFrequent(input, k), expected);
}
