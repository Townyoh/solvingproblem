#include "../pch.h"
#include "../../SolvingProblem/LeetCode/41_FirstMissingPositive.cpp"

TEST(FirstMissingPositive, First)
{
  auto input = vector<int>{ 1,2,0 };
  auto expected = 3;

  FirstMissingPositive::Solution solver;

  EXPECT_EQ(solver.firstMissingPositive(input), expected);
}

TEST(FirstMissingPositive, Second)
{
  auto input = vector<int>{ 3,4,-1,1 };
  auto expected = 2;

  FirstMissingPositive::Solution solver;

  EXPECT_EQ(solver.firstMissingPositive(input), expected);
}

TEST(FirstMissingPositive, Third)
{
  auto input = vector<int>{ 7,8,9,11,12 };
  auto expected = 1;

  FirstMissingPositive::Solution solver;

  EXPECT_EQ(solver.firstMissingPositive(input), expected);
}

TEST(FirstMissingPositive, Fourth)
{
  auto input = vector<int>{ 1,1 };
  auto expected = 2;

  FirstMissingPositive::Solution solver;

  EXPECT_EQ(solver.firstMissingPositive(input), expected);
}
