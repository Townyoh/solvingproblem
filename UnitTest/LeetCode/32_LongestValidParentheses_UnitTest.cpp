#include "../pch.h"
#include "../../SolvingProblem/LeetCode/32_LongestValidParentheses.cpp"

TEST(LongestValidParentheses, First)
{
  auto input = string("(()");
  auto output = 2;

  LongestValidParentheses::Solution solver;

  EXPECT_EQ(solver.longestValidParentheses(input), output);
}

TEST(LongestValidParentheses, Second)
{
  auto input = string(")()())");
  auto output = 4;

  LongestValidParentheses::Solution solver;

  EXPECT_EQ(solver.longestValidParentheses(input), output);
}
