#include "../pch.h"
#include "../../SolvingProblem/LeetCode/329_LongestIncreasingPathInAMatrix.cpp"

TEST(LongestIncreasingPathInAMatrix, First)
{
  auto matrix = vector<vector<int>>
  {
    { 9,9,4 },
    { 6,6,8 },
    { 2,1,1 }
  };

  auto expected = 4;

  LongestIncreasingPathInAMatrix::Solution solver;

  EXPECT_EQ(solver.longestIncreasingPath(matrix), expected);
}

TEST(LongestIncreasingPathInAMatrix, Second)
{
  auto matrix = vector<vector<int>>
  {
    { 3,4,5 },
    { 3,2,6 },
    { 2,2,1 }
  };

  auto expected = 4;

  LongestIncreasingPathInAMatrix::Solution solver;

  EXPECT_EQ(solver.longestIncreasingPath(matrix), expected);
}
