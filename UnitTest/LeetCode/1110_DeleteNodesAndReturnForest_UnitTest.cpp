#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1110_DeleteNodesAndReturnForest.cpp"

TEST(DeleteNodesAndReturnForest, First)
{
  auto toDelete = vector<int>{ 3, 5 };
  TreeNode* root = new TreeNode(1);
  {
    root->left = new TreeNode(2);
    root->right = new TreeNode(3);

    root->left->left = new TreeNode(4);
    root->left->right = new TreeNode(5);

    root->right->left = new TreeNode(6);
    root->right->right = new TreeNode(7);
  }

  TreeNode* resultFirst = new TreeNode(1);
  {
    root->left = new TreeNode(2);
    root->left->left = new TreeNode(4);
  }

  TreeNode* resultSecond = new TreeNode(6);
  TreeNode* resultThird = new TreeNode(7);

  auto result = vector<TreeNode*>
  {
    resultFirst, resultSecond, resultThird
  };

  DeleteNodesAndReturnForest::Solution solver;

  IS_EQUAL_VECTOR_TREE(solver.delNodes(root, toDelete), result);
}

TEST(DeleteNodesAndReturnForest, Second)
{
  auto toDelete = vector<int>{ 2, 3 };
  TreeNode* root = new TreeNode(1);
  {
    root->left = new TreeNode(2);

    root->left->left = new TreeNode(4);
    root->left->right = new TreeNode(3);
  }

  TreeNode* resultFirst = new TreeNode(1);
  TreeNode* resultSecond = new TreeNode(4);

  auto result = vector<TreeNode*>
  {
    resultFirst, resultSecond
  };

  DeleteNodesAndReturnForest::Solution solver;

  IS_EQUAL_VECTOR_TREE(solver.delNodes(root, toDelete), result);
}