#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1030_MatrixCellsInDistanceOrder.cpp"

TEST(MatrixCellsInDistanceOrder, First)
{
  MatrixCellsInDistanceOrder::Solution solver;

  vector<vector<int>> result
  {
    { 0, 0 },
    { 0, 1 }
  };

  EXPECT_TRUE(IS_EQUAL_VECTOR(solver.allCellsDistOrder(1, 2, 0, 0), result));
}

TEST(MatrixCellsInDistanceOrder, Second)
{
  MatrixCellsInDistanceOrder::Solution solver;

  vector<vector<int>> result
  {
    { 0, 1 },
    { 1, 1 },
    { 0, 0 },
    { 1, 0 }
  };

  EXPECT_TRUE(IS_EQUAL_VECTOR(solver.allCellsDistOrder(2, 2, 0, 1), result));
}

TEST(MatrixCellsInDistanceOrder, Third)
{
  MatrixCellsInDistanceOrder::Solution solver;

  vector<vector<int>> result
  {
    { 1, 2 },
    { 0, 2 },
    { 1, 1 },
    { 0, 1 },
    { 1, 0 },
    { 0, 0 }
  };

  EXPECT_TRUE(IS_EQUAL_VECTOR(solver.allCellsDistOrder(2, 3, 1, 2), result));
}