#include "../pch.h"
#include "../../SolvingProblem/LeetCode/57_InsertInterval.cpp"

TEST(InsertInterval, First)
{
  auto intervals = vector<vector<int>>
  {
    { 1,3 },
    { 6,9 }
  };

  auto newInterval = vector<int>{ 2,5 };

  auto expected = vector<vector<int>>
  {
    { 1,5 },
    { 6,9 }
  };

  InsertInterval::Solution solver;

  EXPECT_EQ(solver.insert(intervals, newInterval), expected);
}

TEST(InsertInterval, Second)
{
  auto intervals = vector<vector<int>>
  {
    { 1,2 },
    { 3,5 },
    { 6,7 },
    { 8,10 },
    { 12,16 }
  };

  auto newInterval = vector<int>{ 4,8 };

  auto expected = vector<vector<int>>
  {
    { 1,2 },
    { 3,10 },
    { 12,16 }
  };

  InsertInterval::Solution solver;

  EXPECT_EQ(solver.insert(intervals, newInterval), expected);
}

TEST(InsertInterval, Third)
{
  auto intervals = vector<vector<int>>
  {
    { 1,3 },
    { 6,9 }
  };

  auto newInterval = vector<int>{ 4,5 };

  auto expected = vector<vector<int>>
  {
    { 1,3 },
    { 4,5 },
    { 6,9 }
  };

  InsertInterval::Solution solver;

  EXPECT_EQ(solver.insert(intervals, newInterval), expected);
}

TEST(InsertInterval, Fourth)
{
  auto intervals = vector<vector<int>>
  {
    { 2,3 },
    { 6,9 }
  };

  auto newInterval = vector<int>{ 0,1 };

  auto expected = vector<vector<int>>
  {
    { 0,1 },
    { 2,3 },
    { 6,9 }
  };

  InsertInterval::Solution solver;

  EXPECT_EQ(solver.insert(intervals, newInterval), expected);
}

TEST(InsertInterval, Fifth)
{
  auto intervals = vector<vector<int>>
  {
    { 2,3 },
    { 6,9 }
  };

  auto newInterval = vector<int>{ 10,11 };

  auto expected = vector<vector<int>>
  {
    { 2,3 },
    { 6,9 },
    { 10,11}
  };

  InsertInterval::Solution solver;

  EXPECT_EQ(solver.insert(intervals, newInterval), expected);
}