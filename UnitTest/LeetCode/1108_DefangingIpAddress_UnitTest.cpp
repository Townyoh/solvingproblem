#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1108_DefangingIpAddress.cpp"

TEST(DefangingIpAddress, First)
{
  auto input = std::string("1.1.1.1");
  auto result = std::string("1[.]1[.]1[.]1");

  DefangingIpAddress::Solution solver;

  EXPECT_EQ(solver.defangIPaddr(input), result);
}

TEST(DefangingIpAddress, Second)
{
  auto input = std::string("255.100.50.0");
  auto result = std::string("255[.]100[.]50[.]0");

  DefangingIpAddress::Solution solver;

  EXPECT_EQ(solver.defangIPaddr(input), result);
}

