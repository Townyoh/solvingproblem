#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1324_PrintWordsVertically.cpp"

TEST(PrintWordsVertically, First)
{
  auto input = string("HOW ARE YOU");
  auto expected = vector<string>{ "HAY","ORO","WEU" };

  PrintWordsVertically::Solution solver;

  EXPECT_EQ(solver.printVertically(input), expected);
}

TEST(PrintWordsVertically, Second)
{
  auto input = string("TO BE OR NOT TO BE");
  auto expected = vector<string>{ "TBONTB","OEROOE","   T" };

  PrintWordsVertically::Solution solver;

  EXPECT_EQ(solver.printVertically(input), expected);
}

TEST(PrintWordsVertically, Third)
{
  auto input = string("CONTEST IS COMING");
  auto expected = vector<string>{ "CIC","OSO","N M","T I","E N","S G","T" };

  PrintWordsVertically::Solution solver;

  EXPECT_EQ(solver.printVertically(input), expected);
}
