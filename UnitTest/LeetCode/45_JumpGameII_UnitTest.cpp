#include "../pch.h"
#include "../../SolvingProblem/LeetCode/45_JumpGameII.cpp"

TEST(JumpGameII, First)
{
  auto input = vector<int>{ 2,3,1,1,4 };
  auto expected = 2;

  JumpGameII::Solution solver;
  EXPECT_EQ(solver.jump(input), expected);
}

TEST(JumpGameII, Second)
{
  auto input = vector<int>{ 2,1,1,1,4 };
  auto expected = 3;

  JumpGameII::Solution solver;
  EXPECT_EQ(solver.jump(input), expected);
}
