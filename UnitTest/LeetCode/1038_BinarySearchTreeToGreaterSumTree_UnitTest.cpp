#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1038_BinarySearchTreeToGreaterSumTree.cpp"

TEST(BinarySearchTreeToGreaterSumTree, First)
{
  BinarySearchTreeToGreaterSumTree::Solution solver;
  TreeNode *root = new TreeNode(4);
  
  root->left = new TreeNode(1);
  root->left->left = new TreeNode(0);
  root->left->right = new TreeNode(2);
  root->left->right->right = new TreeNode(3);

  root->right = new TreeNode(6);
  root->right->left = new TreeNode(5);
  root->right->right = new TreeNode(7);
  root->right->right->right = new TreeNode(8);

  TreeNode *result = new TreeNode(30);

  result->left = new TreeNode(36);
  result->left->left = new TreeNode(36);
  result->left->right = new TreeNode(35);
  result->left->right->right = new TreeNode(33);

  result->right = new TreeNode(21);
  result->right->left = new TreeNode(26);
  result->right->right = new TreeNode(15);
  result->right->right->right = new TreeNode(8);

  EXPECT_TRUE(IS_EQUAL_TREE(result, solver.bstToGst(root)));
}