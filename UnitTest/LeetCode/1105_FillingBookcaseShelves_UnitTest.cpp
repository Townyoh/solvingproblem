#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1105_FillingBookcaseShelves.cpp"

TEST(FillingBookcaseShelves, First)
{
  auto data = vector<vector<int>>
  {
    { 1, 1 },
    { 2, 3 },
    { 2, 3 },
    { 1, 1 },
    { 1, 1 },
    { 1, 1 },
    { 1, 2 }
  };
  auto width = 4;
  auto result = 6;

  FillingBookcaseShelves::Solution solver;

  EXPECT_EQ(solver.minHeightShelves(data, width), result);
}