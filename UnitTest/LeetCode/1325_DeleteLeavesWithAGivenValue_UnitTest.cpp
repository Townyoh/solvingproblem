#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1325_DeleteLeavesWithAGivenValue.cpp"

TEST(DeleteLeavesWithAGivenValue, First)
{
  auto root = new TreeNode(1);
  {
    root->left = new TreeNode(2);
    root->left->left = new TreeNode(2);
    root->right = new TreeNode(3);
    root->right->left = new TreeNode(2);
    root->right->right = new TreeNode(4);
  }

  auto expected = new TreeNode(1);
  {
    expected->right = new TreeNode(3);
    expected->right->right = new TreeNode(4);
  }

  DeleteLeavesWithAGivenValue::Solution solver;

  EXPECT_TRUE(IS_EQUAL_TREE(solver.removeLeafNodes(root, 2), expected));
}
