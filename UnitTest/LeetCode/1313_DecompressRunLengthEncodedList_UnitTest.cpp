#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1313_DecompressRunLengthEncodedList.cpp"

TEST(DecompressRunLengthEncodedList, First)
{
  auto input = vector<int>{ 1, 2, 3, 4 };
  auto expected = vector<int>{ 2, 4, 4, 4 };

  DecompressRunLengthEncodedList::Solution solver;

  EXPECT_EQ(solver.decompressRLElist(input), expected);
}