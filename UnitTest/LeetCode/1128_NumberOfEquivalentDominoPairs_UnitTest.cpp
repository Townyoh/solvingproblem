#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1128_NumberOfEquivalentDominoPairs.cpp"

TEST(NumberOfEquivalentDominoPairs, First)
{
  auto dominoes = std::vector<vector<int>>
  {
    { 1,2 },
    { 2,1 },
    { 3,4 },
    { 5,6 }
  };
  auto expected = 1;

  NumberOfEquivalentDominoPairs::Solution solver;
  EXPECT_EQ(solver.numEquivDominoPairs(dominoes), expected);
}

TEST(NumberOfEquivalentDominoPairs, Second)
{
  auto dominoes = std::vector<vector<int>>
  {
    { 1,2 },
    { 2,1 },
    { 3,4 },
    { 5,6 },
    { 2,1 },
    { 2,1 },
    { 2,1 },
    { 5,6 }
  };
  auto expected = 11;

  NumberOfEquivalentDominoPairs::Solution solver;
  EXPECT_EQ(solver.numEquivDominoPairs(dominoes), expected);
}