#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1222_QueensThatCanAttackTheKing.cpp"

TEST(QueensThatCanAttackTheKing, First)
{
  auto queens = vector<vector<int>>
  {
    { 0 ,1 },
    { 1 ,0 },
    { 4 ,0 },
    { 0 ,4 },
    { 3 ,3 },
    { 2 ,4 }
  };
  auto king = vector<int>{ 0, 0 };

  auto expected = vector<vector<int>>
  {
    { 0, 1 },
    { 1, 0 },
    { 3, 3 }
  };

  QueensThatCanAttackTheKing::Solution solver;

  EXPECT_EQ(solver.queensAttacktheKing(queens, king), expected);
}