#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1109_CorporateFlightBookings.cpp"

TEST(CorporateFlightBookings, First)
{
  auto booking = vector<vector<int>>
  {
    { 1, 2, 10 },
    { 2, 3, 20 },
    { 2, 5, 25 }
  };
  auto result = vector<int>{ 10,55,45,25,25 };
  auto nbFlight = 5;

  CorporateFlightBookings::Solution solver;

  EXPECT_EQ(solver.corpFlightBookings(booking, nbFlight), result);
}