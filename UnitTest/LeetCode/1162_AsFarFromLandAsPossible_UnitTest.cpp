#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1162_AsFarFromLandAsPossible.cpp"

TEST(AsFarFromLandAsPossible, First)
{
  auto grid = vector<vector<int>>
  {
    { 1,0,1 },
    { 0,0,0 },
    { 1,0,1 }
  };
  auto expected = 2;

  AsFarFromLandAsPossible::Solution solver;

  EXPECT_EQ(solver.maxDistance(grid), expected);
}

TEST(AsFarFromLandAsPossible, Second)
{
  auto grid = vector<vector<int>>
  {
    { 1,0,0 },
    { 0,0,0 },
    { 0,0,0 }
  };
  auto expected = 4;

  AsFarFromLandAsPossible::Solution solver;

  EXPECT_EQ(solver.maxDistance(grid), expected);
}

TEST(AsFarFromLandAsPossible, Third)
{
  auto grid = vector<vector<int>>
  {
    { 1,1,1 },
    { 1,1,1 },
    { 1,1,1 }
  };
  auto expected = -1;

  AsFarFromLandAsPossible::Solution solver;

  EXPECT_EQ(solver.maxDistance(grid), expected);
}

TEST(AsFarFromLandAsPossible, Fourth)
{
  auto grid = vector<vector<int>>
  {
    { 0,0,0 },
    { 0,0,0 },
    { 0,0,0 }
  };
  auto expected = -1;

  AsFarFromLandAsPossible::Solution solver;

  EXPECT_EQ(solver.maxDistance(grid), expected);
}

