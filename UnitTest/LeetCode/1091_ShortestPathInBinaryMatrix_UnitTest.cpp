#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1091_ShortestPathInBinaryMatrix.cpp"

TEST(ShortestPathInBinaryMatrix, First)
{
  auto grid = vector<vector<int>>
  {
    { 0, 1 }, 
    { 1, 0 }
  };
  auto result = 2;

  ShortestPathInBinaryMatrix::Solution solver;

  EXPECT_EQ(solver.shortestPathBinaryMatrix(grid), result);
}

TEST(ShortestPathInBinaryMatrix, Second)
{
  auto grid = vector<vector<int>>
  {
    { 0, 0, 0 },
    { 1, 1, 0 },
    { 1, 1, 0 }
  };
  auto result = 4;

  ShortestPathInBinaryMatrix::Solution solver;

  EXPECT_EQ(solver.shortestPathBinaryMatrix(grid), result);
}

TEST(ShortestPathInBinaryMatrix, Third)
{
  auto grid = vector<vector<int>>
  {
    {0,1,0,1,0},
    {1,0,0,0,1},
    {0,0,1,1,1},
    {0,0,0,0,0},
    {1,0,1,0,0}
  };
  auto result = 6;

  ShortestPathInBinaryMatrix::Solution solver;

  EXPECT_EQ(solver.shortestPathBinaryMatrix(grid), result);
}

TEST(ShortestPathInBinaryMatrix, Fourth)
{
  auto grid = vector<vector<int>>
  {
    {0,0,0,0,0 },
    {1,1,0,1,0 },
    {0,1,1,1,1 },
    {1,1,1,1,0 },
    {0,1,1,0,0 }
  };
  auto result = -1;

  ShortestPathInBinaryMatrix::Solution solver;

  EXPECT_EQ(solver.shortestPathBinaryMatrix(grid), result);
}
