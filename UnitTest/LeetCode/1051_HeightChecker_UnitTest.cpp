#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1051_HeightChecker.cpp"

TEST(HeightChecker, First)
{
  auto data = vector<int>{ 1, 1, 4, 2, 1, 3 };
  auto result = 3;

  HeightChecker::Solution solver;

  EXPECT_EQ(solver.heightChecker(data), result);
}

