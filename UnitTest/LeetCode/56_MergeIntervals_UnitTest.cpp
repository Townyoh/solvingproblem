#include "../pch.h"
#include "../../SolvingProblem/LeetCode/56_MergeIntervals.cpp"

TEST(MergeIntervals, First)
{
  auto intervals = vector<vector<int>>
  {
    { 1,3 },
    { 2,6 },
    { 8,10 },
    { 15,18 }
  };
  auto expected = vector<vector<int>>
  {
    { 1,6 },
    { 8,10 },
    { 15,18 }
  };

  MergeIntervals::Solution solver;

  EXPECT_EQ(solver.merge(intervals), expected);
}

TEST(MergeIntervals, Second)
{
  auto intervals = vector<vector<int>>
  {
    { 1,4 },
    { 4,5 }
   };
  auto expected = vector<vector<int>>
  {
    { 1,5 }
  };

  MergeIntervals::Solution solver;

  EXPECT_EQ(solver.merge(intervals), expected);
}

TEST(MergeIntervals, Third)
{
  auto intervals = vector<vector<int>>
  {
    { 1,4 },
    { 2,3 }
  };
  auto expected = vector<vector<int>>
  {
    { 1,4 }
  };

  MergeIntervals::Solution solver;

  EXPECT_EQ(solver.merge(intervals), expected);
}
