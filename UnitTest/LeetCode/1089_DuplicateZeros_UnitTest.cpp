#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1089_DuplicateZeros.cpp"

TEST(DuplicateZeros, First)
{
  auto data = vector<int>{ 1,0,2,3,0,4,5,0 };
  auto result = vector<int>{ 1,0,0,2,3,0,0,4 };

  DuplicateZeros::Solution solver;
  solver.duplicateZeros(data);

  EXPECT_EQ(data, result);
}

TEST(DuplicateZeros, Second)
{
  auto data = vector<int>{ 1,2,3 };
  auto result = vector<int>{ 1,2,3 };

  DuplicateZeros::Solution solver;
  solver.duplicateZeros(data);

  EXPECT_EQ(data, result);
}

