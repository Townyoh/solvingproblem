#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1221_SplitAStringInBalancedStrings.cpp"

TEST(SplitAStringInBalancedStrings, First)
{
  auto str = string("RLRRLLRLRL");
  auto expected = 4;

  SplitAStringInBalancedStrings::Solution solver;

  EXPECT_EQ(solver.balancedStringSplit(str), expected);
}

TEST(SplitAStringInBalancedStrings, Second)
{
  auto str = string("RLLLLRRRLR");
  auto expected = 3;

  SplitAStringInBalancedStrings::Solution solver;

  EXPECT_EQ(solver.balancedStringSplit(str), expected);
}

TEST(SplitAStringInBalancedStrings, Third)
{
  auto str = string("LLLLRRRR");
  auto expected = 1;

  SplitAStringInBalancedStrings::Solution solver;

  EXPECT_EQ(solver.balancedStringSplit(str), expected);
}
