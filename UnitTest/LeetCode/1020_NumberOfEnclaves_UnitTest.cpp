#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1020_NumberOfEnclaves.cpp"

TEST(NumberOfEnclaves, First)
{
  NumberOfEnclaves::Solution solver;
  vector<vector<int>> data
  {
    { 0,0,0,0 },
    { 1,0,1,0 },
    { 0,1,1,0 },
    { 0,0,0,0 }
  };

  int result = 3;

  EXPECT_EQ(solver.numEnclaves(data), result);
}

TEST(NumberOfEnclaves, Second)
{
  NumberOfEnclaves::Solution solver;
  vector<vector<int>> data
  {
    { 0,1,1,0 },
    { 0,0,1,0 },
    { 0,0,1,0 },
    { 0,0,0,0 }
  };

  int result = 0;

  EXPECT_EQ(solver.numEnclaves(data), result);
}
