#include "../pch.h"
#include "../../SolvingProblem/LeetCode/1029_TwoCityScheduling.cpp"

TEST(TwoCityScheduling, First)
{
  TwoCityScheduling::Solution solver;
  vector<vector<int>> data
  {
    { 10, 20 },
    { 30, 200 },
    { 400, 50 },
    { 30, 20 }
  };

  EXPECT_EQ(solver.twoCitySchedCost(data), 110);
}

TEST(TwoCityScheduling, Second)
{
  TwoCityScheduling::Solution solver;
  vector<vector<int>> data
  {
    { 10, 20 },
    { 30, 200 },
    { 400, 500 },
    { 300, 200 }
  };

  EXPECT_EQ(solver.twoCitySchedCost(data), 650);
}
