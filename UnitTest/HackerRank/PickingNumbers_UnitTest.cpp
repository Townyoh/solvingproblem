#include "../pch.h"
#include "../../SolvingProblem/HackerRank/PickingNumbers.cpp"

TEST(PickingNumbersUnitTest, First)
{
  vector<int> data { 4, 6, 5, 3, 3, 1 };
  EXPECT_EQ(pickingNumbers(data), 3);
}

TEST(PickingNumbersUnitTest, Second)
{
  vector<int> data{ 1, 2, 2, 3, 1, 2 };
  EXPECT_EQ(pickingNumbers(data), 5);
}

TEST(PickingNumbersUnitTest, Third)
{
  vector<int> data{ 1, 1, 2, 2, 4, 4, 5, 5, 5 };
  EXPECT_EQ(pickingNumbers(data), 5);
}


