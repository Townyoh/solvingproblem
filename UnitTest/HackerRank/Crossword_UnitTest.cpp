#include "../pch.h"
#include "../../SolvingProblem/HackerRank/Crossword.cpp"

TEST(CrosswordUnitTest, First)
{
  vector<string> crossword 
  {
    {"+-++++++++"},
    {"+-++++++++"},
    {"+-++++++++"},
    {"+-----++++"},
    {"+-+++-++++"},
    {"+-+++-++++"},
    {"+++++-++++"},
    {"++------++"},
    {"+++++-++++"},
    {"+++++-++++"}
  };
  string words{ "LONDON; DELHI; ICELAND; ANKARA" };
  vector<string> result
  {
    {"+L++++++++"},
    {"+O++++++++"},
    {"+N++++++++"},
    {"+DELHI++++"},
    {"+O+++C++++"},
    {"+N+++E++++"},
    {"+++++L++++"},
    {"++ANKARA++"},
    {"+++++N++++"},
    {"+++++D++++"}
  };

  EXPECT_TRUE(IS_EQUAL_VECTOR(crosswordPuzzle(crossword, words), result));
}

TEST(CrosswordUnitTest, Second)
{
  vector<string> crossword
  {
    {"+-++++++++"},
    {"+-++++++++"},
    {"+-------++"},
    {"+-++++++++"},
    {"+-++++++++"},
    {"+------+++"},
    {"+-+++-++++"},
    {"+++++-++++"},
    {"+++++-++++"},
    {"++++++++++"}
  };
  string words{ "AGRA; NORWAY; ENGLAND; GWALIOR" };
  vector<string> result
  {
    {"+E++++++++"},
    {"+N++++++++"},
    {"+GWALIOR++"},
    {"+L++++++++"},
    {"+A++++++++"},
    {"+NORWAY+++"},
    {"+D+++G++++"},
    {"+++++R++++"},
    {"+++++A++++"},
    {"++++++++++"}
  };

  EXPECT_TRUE(IS_EQUAL_VECTOR(crosswordPuzzle(crossword, words), result));
}

TEST(CrosswordUnitTest, Third)
{
  vector<string> crossword
  {
    {"++++++-+++"},
    {"++------++"},
    {"++++++-+++"},
    {"++++++-+++"},
    {"+++------+"},
    {"++++++-+-+"},
    {"++++++-+-+"},
    {"++++++++-+"},
    {"++++++++-+"},
    {"++++++++-+"}
  };
  string words{ "ICELAND ;MEXICO; PANAMA; ALMATY" };
  vector<string> result
  {
    {"++++++I+++"},
    {"++MEXICO++"},
    {"++++++E+++"},
    {"++++++L+++"},
    {"+++PANAMA+"},
    {"++++++N+L+"},
    {"++++++D+M+"},
    {"++++++++A+"},
    {"++++++++T+"},
    {"++++++++Y+"}
  };

  EXPECT_TRUE(IS_EQUAL_VECTOR(crosswordPuzzle(crossword, words), result));
}

TEST(CrosswordUnitTest, Fourth)
{
  vector<string> crossword
  {
    {"+-++++++++"},
    {"+-++-+++++"},
    {"+-------++"},
    {"+-++-+++++"},
    {"+-++-+++++"},
    {"+-++-+++++"},
    {"++++-+++++"},
    {"++++-+++++"},
    {"++++++++++"},
    {"----------"}
  };
  string words{ "CALIFORNIA; NIGERIA; CANADA; TELAVIV" };
  vector<string> result
  {
    {"+C++++++++"},
    {"+A++T+++++"},
    {"+NIGERIA++"},
    {"+A++L+++++"},
    {"+D++A+++++"},
    {"+A++V+++++"},
    {"++++I+++++"},
    {"++++V+++++"},
    {"++++++++++"},
    {"CALIFORNIA"}
  };

  EXPECT_TRUE(IS_EQUAL_VECTOR(crosswordPuzzle(crossword, words), result));
}