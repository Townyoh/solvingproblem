//
// pch.h
// Header for standard system include files.
//

#pragma once
#include "gtest/gtest.h"

using namespace std;

template<typename T>
::testing::AssertionResult IS_EQUAL_VECTOR(const vector<T>& first, const vector<T>& second)
{
  if (first.size() != second.size())
    return ::testing::AssertionFailure();

  for (int index = 0; index < first.size(); index++)
  {
    for (int i = 0; i < first[index].size(); i++)
    {
      if (first[index][i] != second[index][i])
        return ::testing::AssertionFailure();
    }
  }

  return ::testing::AssertionSuccess();
}

struct TreeNode;
struct ListNode;

::testing::AssertionResult IS_EQUAL_TREE(TreeNode* ref, TreeNode* compare);
::testing::AssertionResult IS_EQUAL_VECTOR_TREE(const vector<TreeNode*>& first, const vector<TreeNode*>& second);
::testing::AssertionResult IS_EQUAL_LINKED_LIST(ListNode* ref, ListNode* compare);

